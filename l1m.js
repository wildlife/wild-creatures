(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [
		{name:"l1m_atlas_1", frames: [[0,1081,870,409],[872,1081,500,405],[0,629,1200,450],[1202,826,300,164],[0,0,1314,627],[1202,629,300,195]]},
		{name:"l1m_atlas_2", frames: [[0,0,1500,1115],[0,1117,1600,906]]},
		{name:"l1m_atlas_3", frames: [[0,0,1500,1116]]},
		{name:"l1m_atlas_4", frames: [[0,0,1500,1357]]}
];


(lib.AnMovieClip = function(){
	this.actionFrames = [];
	this.ignorePause = false;
	this.gotoAndPlay = function(positionOrLabel){
		cjs.MovieClip.prototype.gotoAndPlay.call(this,positionOrLabel);
	}
	this.play = function(){
		cjs.MovieClip.prototype.play.call(this);
	}
	this.gotoAndStop = function(positionOrLabel){
		cjs.MovieClip.prototype.gotoAndStop.call(this,positionOrLabel);
	}
	this.stop = function(){
		cjs.MovieClip.prototype.stop.call(this);
	}
}).prototype = p = new cjs.MovieClip();
// symbols:



(lib.alley = function() {
	this.initialize(ss["l1m_atlas_2"]);
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.animals8 = function() {
	this.initialize(ss["l1m_atlas_4"]);
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.city = function() {
	this.initialize(ss["l1m_atlas_1"]);
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.cloud_1 = function() {
	this.initialize(ss["l1m_atlas_1"]);
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.cloud_2 = function() {
	this.initialize(ss["l1m_atlas_1"]);
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.cloud_3 = function() {
	this.initialize(ss["l1m_atlas_1"]);
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.man = function() {
	this.initialize(ss["l1m_atlas_1"]);
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.owl = function() {
	this.initialize(ss["l1m_atlas_1"]);
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.sky_alley = function() {
	this.initialize(ss["l1m_atlas_3"]);
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.sky_city_perspective = function() {
	this.initialize(ss["l1m_atlas_2"]);
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.Tween15 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.man();
	this.instance.setTransform(-378,-181,0.5759,0.5759);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-378,-181,756.8,361.1);


(lib.Tween14 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.alley();
	this.instance.setTransform(-694.75,-516.45,0.9264,0.9264);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-694.7,-516.4,1389.5,1032.9);


(lib.Tween13 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.sky_alley();
	this.instance.setTransform(-750,-558);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-750,-558,1500,1116);


(lib.Tween12 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.cloud_1();
	this.instance.setTransform(-250,-202.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-250,-202.5,500,405);


(lib.Tween11 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.cloud_2();
	this.instance.setTransform(-665,-249,1.1079,1.1079);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-665,-249,1329.5,498.6);


(lib.Tween10 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.cloud_3();
	this.instance.setTransform(-150,-82);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-150,-82,300,164);


(lib.Tween8 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.cloud_3();
	this.instance.setTransform(-351.95,-192.4,2.3463,2.3463);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-351.9,-192.4,703.9,384.8);


(lib.Tween7 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.owl();
	this.instance.setTransform(-88,-57,0.5846,0.5846);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-88,-57,175.4,114);


(lib.Tween5 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.sky_city_perspective();
	this.instance.setTransform(-618,-448,1.271,1.271);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-618,-448,2033.7,1151.6);


(lib.Tween3 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.city();
	this.instance.setTransform(-473,-513,1.639,1.639);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-473,-513,1426,670.4);


(lib.Tween1 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.animals8();
	this.instance.setTransform(-28,-377,0.5559,0.5559);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-28,-377,833.8,754.4);


(lib.___Camera___ = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// timeline functions:
	this.frame_0 = function() {
		this.visible = false;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// cameraBoundary
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,0,0,0)").ss(2,1,1,3,true).p("EAq+AfQMhV7AAAMAAAg+fMBV7AAAg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(2));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-188.5,-407,377,814);


(lib.Scene_1_Sky = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Sky
	this.instance = new lib.Tween5("synched",0);
	this.instance.setTransform(-665.3,-269.55,7.995,7.995);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regY:-0.1,scaleX:5.2461,scaleY:5.2461,x:-1456.35,y:553.5},74).wait(11));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_owl = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// owl
	this.instance = new lib.Tween7("synched",0);
	this.instance.setTransform(-214.95,1713.05,0.6928,0.6928,-36.7283);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(79).to({_off:false},0).wait(1).to({regX:-0.3,rotation:-34.5152,x:-202.1,y:1685.45},0).wait(1).to({scaleX:0.6929,scaleY:0.6929,rotation:-32.3019,x:-187.6,y:1656.15},0).wait(1).to({scaleX:0.6928,scaleY:0.6928,rotation:-30.0885,x:-172.1,y:1626.25},0).wait(1).to({rotation:-27.8751,x:-156.15,y:1596.1},0).wait(1).to({rotation:-25.6618,x:-139.8,y:1566},0).wait(1).to({rotation:-23.4484,x:-123.3,y:1536.05},0).wait(1).to({rotation:-21.2351,x:-106.65,y:1506.45},0).wait(1).to({rotation:-19.0217,x:-89.95,y:1477.15},0).wait(1).to({scaleX:0.6929,scaleY:0.6929,rotation:-16.8083,x:-73.3,y:1448.35},0).wait(1).to({scaleX:0.6928,scaleY:0.6928,rotation:-14.595,x:-56.6,y:1420.05},0).wait(1).to({rotation:-12.3816,x:-40,y:1392.35},0).wait(1).to({scaleX:0.6929,scaleY:0.6929,rotation:-10.1682,x:-23.5,y:1365.2},0).wait(1).to({scaleX:0.6928,scaleY:0.6928,rotation:-7.9549,x:-7.15,y:1338.75},0).wait(1).to({scaleX:0.6929,scaleY:0.6929,rotation:-5.7415,x:9.05,y:1312.9},0).wait(1).to({scaleX:0.6928,scaleY:0.6928,rotation:-3.5281,x:25.1,y:1287.85},0).wait(1).to({scaleX:0.6929,scaleY:0.6929,rotation:-1.3148,x:41,y:1263.6},0).wait(1).to({scaleX:0.6928,scaleY:0.6928,rotation:0.8986,x:56.7,y:1240.15},0).wait(1).to({scaleX:0.6929,scaleY:0.6929,rotation:3.112,x:72.15,y:1217.6},0).wait(1).to({scaleX:0.6928,scaleY:0.6928,rotation:5.3253,x:87.35,y:1196},0).wait(1).to({scaleX:0.6929,scaleY:0.6929,rotation:7.5387,x:102.25,y:1175.35},0).wait(1).to({rotation:8.2878,x:116.9,y:1155.8},0).wait(1).to({scaleX:0.6928,scaleY:0.6928,rotation:9.0369,x:131.25,y:1137.35},0).wait(1).to({rotation:9.7861,x:145.2,y:1120.1},0).wait(1).to({rotation:10.5352,x:158.8,y:1104.15},0).wait(1).to({scaleX:0.6929,scaleY:0.6929,rotation:11.2843,x:172.05,y:1090.15},0).wait(1).to({rotation:12.0334,x:185,y:1078.55},0).wait(1).to({rotation:12.7826,x:197.65,y:1069},0).wait(1).to({scaleX:0.6928,scaleY:0.6928,rotation:13.5317,x:210.1,y:1061.4},0).wait(1).to({scaleX:0.6929,scaleY:0.6929,rotation:14.2808,x:222.35,y:1055.55},0).wait(1).to({scaleX:0.6928,scaleY:0.6928,rotation:15.0299,x:234.4,y:1051.35},0).wait(1).to({rotation:15.7791,x:246.25,y:1048.7},0).wait(1).to({rotation:16.5282,x:258,y:1047.65},0).wait(1).to({rotation:17.2773,x:269.6,y:1048.1},0).wait(1).to({rotation:18.0264,x:281.1,y:1050.1},0).wait(1).to({rotation:18.7756,x:292.45,y:1053.75},0).wait(1).to({rotation:19.5247,x:303.7,y:1059.2},0).wait(1).to({rotation:20.2738,x:314.85,y:1066.65},0).wait(1).to({rotation:21.0229,x:325.9,y:1076.65},0).wait(1).to({scaleX:0.6929,scaleY:0.6929,rotation:21.7721,x:336.9,y:1090.1},0).wait(1).to({scaleX:0.6928,scaleY:0.6928,rotation:22.5212,x:347.85,y:1109.5},0).wait(1).to({rotation:23.2703,x:359.3,y:1160.5},0).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_cloud6dup = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// cloud6dup
	this.instance = new lib.Tween12("synched",0);
	this.instance.setTransform(-330.3,1930.8,1.9688,1.9688,0,0,0,-0.1,0.1);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(53).to({_off:false},0).to({x:2175,y:928.6},55).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_cloud6 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// cloud6
	this.instance = new lib.Tween12("synched",0);
	this.instance.setTransform(1530.8,2125.65,1.9688,1.9688,0,0,0,-0.1,0.1);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(59).to({_off:false},0).to({regY:0.2,x:1726.6,y:1380.6},16).to({scaleX:1.9687,scaleY:1.9687,x:2368.75,y:989},12).to({regY:0.1,scaleX:1.9688,scaleY:1.9688,x:2665.1,y:808.25},20).wait(2));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_cloud3 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// cloud3
	this.instance = new lib.Tween10("synched",0);
	this.instance.setTransform(889.7,1902.3,1.9091,1.9091);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(59).to({_off:false},0).to({scaleX:1.909,scaleY:1.909,x:1202.95,y:1021.15},28).to({x:1256.95,y:869.25},16).to({scaleX:1.9091,scaleY:1.9091,x:1267.8,y:838.9},4).wait(2));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Cloud2dup = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Cloud2dup
	this.instance = new lib.Tween11("synched",0);
	this.instance.setTransform(-377.65,1964.5,1.4668,1.4668);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(59).to({_off:false},0).to({scaleX:1.4326,scaleY:1.4326,x:-966.05,y:1447.1},9).to({scaleX:1.3885,scaleY:1.3885,x:814.15,y:908.5},19).to({scaleX:1.3775,scaleY:1.3775,x:1259.2,y:773.85},16).to({scaleX:1.3749,scaleY:1.3749,x:1942.15,y:790.85},4).wait(2));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Cloud2 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Cloud2
	this.instance = new lib.Tween11("synched",0);
	this.instance.setTransform(-377.65,1964.5,1.4668,1.4668);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(59).to({_off:false},0).to({scaleX:1.4379,scaleY:1.4379,x:673.6,y:1476.6},11).to({scaleX:1.3906,scaleY:1.3906,x:1625.05,y:962.3},17).to({scaleX:1.3775,scaleY:1.3775,x:1889.3,y:819.4},16).to({scaleX:1.3749,scaleY:1.3749,x:1942.15,y:790.85},4).wait(2));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Cloud = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Cloud
	this.instance = new lib.Tween8("synched",0);
	this.instance.setTransform(-360.1,1055.05);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(80).to({_off:false},0).to({x:-3.9,y:980.7},7).to({x:129.7,y:952.85},10).to({x:572.5,y:769.4},10).to({startPosition:0},2).to({_off:true},1).wait(94));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_City = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// City
	this.instance = new lib.Tween3("synched",0);
	this.instance.setTransform(-834.85,2854.8,4.6346,4.6346);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(74));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_bus = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// bus
	this.instance = new lib.Tween15("synched",0);
	this.instance.setTransform(-1458.1,4110.65,1.4238,1.4238,0,0,0,-0.2,0.1);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(127).to({_off:false},0).wait(1).to({regX:0.4,regY:-0.5,scaleX:1.4221,scaleY:1.4221,x:-1431.85,y:4110},0).wait(1).to({scaleX:1.4204,scaleY:1.4204,x:-1402.85,y:4110.25},0).wait(1).to({scaleX:1.4186,scaleY:1.4186,x:-1370.4,y:4110.45},0).wait(1).to({scaleX:1.4169,scaleY:1.4169,x:-1334.85,y:4110.7},0).wait(1).to({scaleX:1.4152,scaleY:1.4152,x:-1296.45,y:4110.9},0).wait(1).to({scaleX:1.4134,scaleY:1.4134,x:-1255.45,y:4111.15},0).wait(1).to({scaleX:1.4117,scaleY:1.4117,x:-1212.15,y:4111.35},0).wait(1).to({scaleX:1.4099,scaleY:1.4099,x:-1166.9,y:4111.6},0).wait(1).to({scaleX:1.4082,scaleY:1.4082,x:-1120,y:4111.8},0).wait(1).to({scaleX:1.4065,scaleY:1.4065,x:-1071.75,y:4112.05},0).wait(1).to({scaleX:1.4047,scaleY:1.4047,x:-1022.5,y:4112.25},0).wait(1).to({scaleX:1.403,scaleY:1.403,x:-972.55,y:4112.5},0).wait(1).to({scaleX:1.4013,scaleY:1.4013,x:-922.3,y:4112.7},0).wait(1).to({scaleX:1.3995,scaleY:1.3995,x:-872.05,y:4112.95},0).wait(1).to({scaleX:1.3978,scaleY:1.3978,x:-822.1,y:4113.15},0).wait(1).to({scaleX:1.3961,scaleY:1.3961,x:-772.85,y:4113.4},0).wait(1).to({scaleX:1.3943,scaleY:1.3943,x:-724.6,y:4113.6},0).wait(1).to({scaleX:1.3926,scaleY:1.3926,x:-677.7,y:4113.85},0).wait(1).to({scaleX:1.3909,scaleY:1.3909,x:-632.45,y:4114.05},0).wait(1).to({scaleX:1.3891,scaleY:1.3891,x:-589.15,y:4114.3},0).wait(1).to({scaleX:1.3874,scaleY:1.3874,x:-548.15,y:4114.5},0).wait(1).to({scaleX:1.3857,scaleY:1.3857,x:-509.7,y:4114.75},0).wait(1).to({scaleX:1.3839,scaleY:1.3839,x:-474.2,y:4114.95},0).wait(1).to({scaleX:1.3822,scaleY:1.3822,x:-441.75,y:4115.2},0).wait(1).to({scaleX:1.3804,scaleY:1.3804,x:-412.75,y:4115.4},0).wait(1).to({scaleX:1.3787,scaleY:1.3787,x:-387.35,y:4115.65},0).wait(1).to({scaleX:1.377,scaleY:1.377,x:-364.45,y:4115.85},0).wait(1).to({scaleX:1.3752,scaleY:1.3752,x:-342.65,y:4116.1},0).wait(1).to({scaleX:1.3735,scaleY:1.3735,x:-321.85,y:4116.3},0).wait(1).to({scaleX:1.3718,scaleY:1.3718,x:-302.05,y:4116.55},0).wait(1).to({scaleX:1.37,scaleY:1.37,x:-283.15,y:4116.75},0).wait(1).to({scaleX:1.3683,scaleY:1.3683,x:-265.15,y:4117},0).wait(1).to({scaleX:1.3666,scaleY:1.3666,x:-248,y:4117.2},0).wait(1).to({scaleX:1.3648,scaleY:1.3648,x:-231.65,y:4117.45},0).wait(1).to({scaleX:1.3631,scaleY:1.3631,x:-216.05,y:4117.65},0).wait(1).to({scaleX:1.3614,scaleY:1.3614,x:-201.1,y:4117.9},0).wait(1).to({scaleX:1.3596,scaleY:1.3596,x:-186.85,y:4118.1},0).wait(1).to({scaleX:1.3579,scaleY:1.3579,x:-173.15,y:4118.35},0).wait(1).to({scaleX:1.3562,scaleY:1.3562,x:-160.05,y:4118.55},0).wait(1).to({scaleX:1.3544,scaleY:1.3544,x:-147.5,y:4118.8},0).wait(1).to({scaleX:1.3527,scaleY:1.3527,x:-135.35,y:4119},0).wait(1).to({scaleX:1.351,scaleY:1.351,x:-123.7,y:4119.25},0).wait(1).to({scaleX:1.3492,scaleY:1.3492,x:-112.4,y:4119.5},0).wait(1).to({scaleX:1.3475,scaleY:1.3475,x:-101.45,y:4119.75},0).wait(1).to({scaleX:1.3458,scaleY:1.3458,x:-90.8,y:4119.95},0).wait(1).to({scaleX:1.344,scaleY:1.344,x:-80.4,y:4120.2},0).wait(1).to({scaleX:1.3423,scaleY:1.3423,x:-70.2,y:4120.4},0).wait(1).to({scaleX:1.3406,scaleY:1.3406,x:-60.2,y:4120.65},0).wait(1).to({scaleX:1.3388,scaleY:1.3388,x:-50.3,y:4120.85},0).wait(1).to({scaleX:1.3371,scaleY:1.3371,x:-40.5,y:4121.1},0).wait(1).to({scaleX:1.3354,scaleY:1.3354,x:-30.75,y:4121.3},0).wait(1).to({scaleX:1.3337,scaleY:1.3337,x:-20.95,y:4121.55},0).wait(1).to({scaleX:1.3319,scaleY:1.3319,x:-11.2,y:4121.75},0).wait(1).to({scaleX:1.3302,scaleY:1.3302,x:-1.3,y:4122},0).wait(1).to({scaleX:1.3285,scaleY:1.3285,x:8.7,y:4122.2},0).wait(1).to({scaleX:1.3267,scaleY:1.3267,x:18.85,y:4122.45},0).wait(1).to({scaleX:1.325,scaleY:1.325,x:29.25,y:4122.65},0).wait(1).to({scaleX:1.3233,scaleY:1.3233,x:39.9,y:4122.9},0).wait(1).to({scaleX:1.3215,scaleY:1.3215,x:50.85,y:4123.1},0).wait(1).to({scaleX:1.3198,scaleY:1.3198,x:62.2,y:4123.35},0).wait(1).to({scaleX:1.3181,scaleY:1.3181,x:73.85,y:4123.55},0).wait(1).to({scaleX:1.3164,scaleY:1.3164,x:85.95,y:4123.8},0).wait(1).to({scaleX:1.3146,scaleY:1.3146,x:98.55,y:4124},0).wait(1).to({scaleX:1.3129,scaleY:1.3129,x:111.65,y:4124.25},0).wait(1).to({scaleX:1.3112,scaleY:1.3112,x:125.25,y:4124.45},0).wait(1).to({scaleX:1.3094,scaleY:1.3094,x:139.55,y:4124.7},0).wait(1).to({scaleX:1.3077,scaleY:1.3077,x:154.45,y:4124.9},0).wait(1).to({scaleX:1.306,scaleY:1.306,x:170.05,y:4125.15},0).wait(1).to({scaleX:1.3042,scaleY:1.3042,x:186.45,y:4125.35},0).wait(1).to({scaleX:1.3025,scaleY:1.3025,x:203.6,y:4125.6},0).wait(1).to({scaleX:1.3008,scaleY:1.3008,x:221.55,y:4125.8},0).wait(1).to({scaleX:1.2991,scaleY:1.2991,x:240.45,y:4126.05},0).wait(1).to({scaleX:1.2973,scaleY:1.2973,x:260.25,y:4126.25},0).wait(1).to({scaleX:1.2956,scaleY:1.2956,x:281.05,y:4126.5},0).wait(1).to({scaleX:1.2939,scaleY:1.2939,x:302.85,y:4126.7},0).wait(1).to({scaleX:1.2921,scaleY:1.2921,x:325.8,y:4126.95},0).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_bsky = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// bsky
	this.instance = new lib.Tween13("synched",0);
	this.instance.setTransform(862.7,2730.1,1.3595,1.3595);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(59).to({_off:false},0).to({scaleX:1.3594,scaleY:1.3594,x:897.35,y:2110.1},20).to({scaleX:1.3595,scaleY:1.3595,x:923.3,y:1645.3},15).to({scaleX:5.9892,scaleY:5.9892,x:955.15,y:2163.55},8).to({scaleX:24.7582,scaleY:24.7582,x:1230.05,y:1032.5},14).wait(88));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Animals = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Animals
	this.instance = new lib.Tween1("synched",0);
	this.instance.setTransform(-418,1946.4,1.5085,1.5085,0,0,0,-0.1,0.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(74).to({regX:0,regY:0,scaleX:2.3127,scaleY:2.3127,x:69.9,y:1870.25},0).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Alley = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Alley
	this.instance = new lib.Tween14("synched",0);
	this.instance.setTransform(3.45,3899.4,1.9765,1.9765,0,0,0,0.4,0.1);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(102).to({_off:false},0).wait(102));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();


// stage content:
(lib.l1m = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	this.actionFrames = [203];
	this.___GetDepth___ = function(obj) {
		var depth = obj.depth;
		var cameraObj = this.___camera___instance;
		if(cameraObj && cameraObj.depth && obj.isAttachedToCamera)
		{
			depth += depth + cameraObj.depth;
		}
		return depth;
		}
	this.___needSorting___ = function() {
		for (var i = 0; i < this.numChildren - 1; i++)
		{
			var prevDepth = this.___GetDepth___(this.getChildAt(i));
			var nextDepth = this.___GetDepth___(this.getChildAt(i + 1));
			if (prevDepth < nextDepth)
				return true;
		}
		return false;
	}
	this.___sortFunction___ = function(obj1, obj2) {
		return (this.exportRoot.___GetDepth___(obj2) - this.exportRoot.___GetDepth___(obj1));
	}
	this.on('tick', function (event){
		var curTimeline = event.currentTarget;
		if (curTimeline.___needSorting___()){
			this.sortChildren(curTimeline.___sortFunction___);
		}
	});

	// timeline functions:
	this.frame_203 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(203).call(this.frame_203).wait(1));

	// Camera
	this.___camera___instance = new lib.___Camera___();
	this.___camera___instance.name = "___camera___instance";
	this.___camera___instance.setTransform(187.5,406);
	this.___camera___instance.depth = 0;
	this.___camera___instance.visible = false;

	this.timeline.addTween(cjs.Tween.get(this.___camera___instance).to({x:142.65,y:1360.4},73).wait(45).to({x:19.75,y:3432.35},12).wait(74));

	// owl_obj_
	this.owl = new lib.Scene_1_owl();
	this.owl.name = "owl";
	this.owl.depth = 0;
	this.owl.isAttachedToCamera = 0
	this.owl.isAttachedToMask = 0
	this.owl.layerDepth = 0
	this.owl.layerIndex = 0
	this.owl.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.owl).wait(79).to({regX:-44.9,regY:954.4,x:-0.05},0).wait(1).to({regX:71.6,regY:1386.7,x:116.45,y:432.3},0).wait(40).to({_off:true},1).wait(83));

	// bus_obj_
	this.bus = new lib.Scene_1_bus();
	this.bus.name = "bus";
	this.bus.setTransform(-0.05,-0.05,0.6594,0.6594,0,0,0,-96.8,-209.7);
	this.bus.depth = 272.8;
	this.bus.isAttachedToCamera = 0
	this.bus.isAttachedToMask = 0
	this.bus.layerDepth = 0
	this.bus.layerIndex = 1
	this.bus.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.bus).wait(127).to({regX:-302.6,regY:396.2,scaleX:0.4211,scaleY:0.4211,x:0,y:0,depth:726.3},0).wait(1).to({regX:-590.6,regY:4109.9,scaleX:1,scaleY:1,x:-288.05,y:3713.8},0).wait(76));

	// Alley_obj_
	this.Alley = new lib.Scene_1_Alley();
	this.Alley.name = "Alley";
	this.Alley.depth = 0;
	this.Alley.isAttachedToCamera = 0
	this.Alley.isAttachedToMask = 0
	this.Alley.layerDepth = 0
	this.Alley.layerIndex = 2
	this.Alley.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Alley).wait(102).to({regX:-504.8,regY:-41.8,scaleX:0.2895,scaleY:0.2895,depth:1296.2},0).wait(102));

	// cloud6dup_obj_
	this.cloud6dup = new lib.Scene_1_cloud6dup();
	this.cloud6dup.name = "cloud6dup";
	this.cloud6dup.depth = 0;
	this.cloud6dup.isAttachedToCamera = 0
	this.cloud6dup.isAttachedToMask = 0
	this.cloud6dup.layerDepth = 0
	this.cloud6dup.layerIndex = 3
	this.cloud6dup.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.cloud6dup).wait(53).to({regX:-44.9,regY:954.4,x:-0.05},55).to({_off:true},1).wait(95));

	// Cloud_obj_
	this.Cloud = new lib.Scene_1_Cloud();
	this.Cloud.name = "Cloud";
	this.Cloud.depth = 0;
	this.Cloud.isAttachedToCamera = 0
	this.Cloud.isAttachedToMask = 0
	this.Cloud.layerDepth = 0
	this.Cloud.layerIndex = 4
	this.Cloud.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Cloud).wait(80).to({regX:-44.9,regY:954.4,x:-0.05},0).wait(124));

	// Cloud2_obj_
	this.Cloud2 = new lib.Scene_1_Cloud2();
	this.Cloud2.name = "Cloud2";
	this.Cloud2.depth = 0;
	this.Cloud2.isAttachedToCamera = 0
	this.Cloud2.isAttachedToMask = 0
	this.Cloud2.layerDepth = 0
	this.Cloud2.layerIndex = 5
	this.Cloud2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Cloud2).wait(70).to({regX:-44.9,regY:954.4,x:-0.05},17).wait(20).to({_off:true},2).wait(95));

	// Cloud2dup_obj_
	this.Cloud2dup = new lib.Scene_1_Cloud2dup();
	this.Cloud2dup.name = "Cloud2dup";
	this.Cloud2dup.depth = 0;
	this.Cloud2dup.isAttachedToCamera = 0
	this.Cloud2dup.isAttachedToMask = 0
	this.Cloud2dup.layerDepth = 0
	this.Cloud2dup.layerIndex = 6
	this.Cloud2dup.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Cloud2dup).wait(68).to({regX:-44.9,regY:954.4,x:-0.05},19).wait(20).to({_off:true},2).wait(95));

	// cloud3_obj_
	this.cloud3 = new lib.Scene_1_cloud3();
	this.cloud3.name = "cloud3";
	this.cloud3.depth = 0;
	this.cloud3.isAttachedToCamera = 0
	this.cloud3.isAttachedToMask = 0
	this.cloud3.layerDepth = 0
	this.cloud3.layerIndex = 7
	this.cloud3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.cloud3).wait(59).to({regX:-44.9,regY:954.4,x:-0.05},28).wait(20).to({_off:true},2).wait(95));

	// cloud6_obj_
	this.cloud6 = new lib.Scene_1_cloud6();
	this.cloud6.name = "cloud6";
	this.cloud6.depth = 0;
	this.cloud6.isAttachedToCamera = 0
	this.cloud6.isAttachedToMask = 0
	this.cloud6.layerDepth = 0
	this.cloud6.layerIndex = 8
	this.cloud6.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.cloud6).wait(59).to({regX:-44.9,regY:954.4,x:-0.05},16).wait(32).to({_off:true},2).wait(95));

	// bsky_obj_
	this.bsky = new lib.Scene_1_bsky();
	this.bsky.name = "bsky";
	this.bsky.depth = 0;
	this.bsky.isAttachedToCamera = 0
	this.bsky.isAttachedToMask = 0
	this.bsky.layerDepth = 0
	this.bsky.layerIndex = 9
	this.bsky.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.bsky).wait(59).to({regX:-44.9,regY:954.4,x:-0.05},20).wait(15).to({regX:-683.4,regY:-428,scaleX:0.227,scaleY:0.227,x:0,depth:1798.95},8).to({regX:-3272.2,regY:-6033.6,scaleX:0.0549,scaleY:0.0549,x:0.05,y:0.1,depth:9092},14).wait(88));

	// Animals_obj_
	this.Animals = new lib.Scene_1_Animals();
	this.Animals.name = "Animals";
	this.Animals.setTransform(168.75,1946.35,0.3229,0.3229,0,0,0,129.6,5177);
	this.Animals.depth = 1107.9;
	this.Animals.isAttachedToCamera = 0
	this.Animals.isAttachedToMask = 0
	this.Animals.layerDepth = 0
	this.Animals.layerIndex = 10
	this.Animals.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Animals).wait(74).to({regX:106.5,regY:4339.9,scaleX:0.517,scaleY:0.517,y:1946.3,depth:493.5},0).to({_off:true},1).wait(129));

	// City_obj_
	this.City = new lib.Scene_1_City();
	this.City.name = "City";
	this.City.setTransform(277.35,2030.65,0.2157,0.2157,0,0,0,604.2,7936.5);
	this.City.depth = 1920.2;
	this.City.isAttachedToCamera = 0
	this.City.isAttachedToMask = 0
	this.City.layerDepth = 0
	this.City.layerIndex = 11
	this.City.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.City).to({_off:true},74).wait(130));

	// Sky_obj_
	this.Sky = new lib.Scene_1_Sky();
	this.Sky.name = "Sky";
	this.Sky.setTransform(2523.1,751.95,0.162,0.162,0,0,0,14604.6,2541.7);
	this.Sky.depth = 2731.95;
	this.Sky.isAttachedToCamera = 0
	this.Sky.isAttachedToMask = 0
	this.Sky.layerDepth = 0
	this.Sky.layerIndex = 12
	this.Sky.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Sky).to({regX:11718.6,regY:3075.3,scaleX:0.2018,scaleY:0.2018,x:2523.3,y:752,depth:2089.5},74).to({_off:true},11).wait(119));

	this._renderFirstFrame();

}).prototype = p = new lib.AnMovieClip();
p.nominalBounds = new cjs.Rectangle(-17151,-12376.5,36949.7,27224.1);
// library properties:
lib.properties = {
	id: '6F9624C44C354D5490851EA990C6744B',
	width: 375,
	height: 812,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"images/l1m_atlas_1.png", id:"l1m_atlas_1"},
		{src:"images/l1m_atlas_2.png", id:"l1m_atlas_2"},
		{src:"images/l1m_atlas_3.png", id:"l1m_atlas_3"},
		{src:"images/l1m_atlas_4.png", id:"l1m_atlas_4"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['6F9624C44C354D5490851EA990C6744B'] = {
	getStage: function() { return exportRoot.stage; },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}

p._getProjectionMatrix = function(container, totalDepth) {	var focalLength = 528.25;
	var projectionCenter = { x : lib.properties.width/2, y : lib.properties.height/2 };
	var scale = (totalDepth + focalLength)/focalLength;
	var scaleMat = new createjs.Matrix2D;
	scaleMat.a = 1/scale;
	scaleMat.d = 1/scale;
	var projMat = new createjs.Matrix2D;
	projMat.tx = -projectionCenter.x;
	projMat.ty = -projectionCenter.y;
	projMat = projMat.prependMatrix(scaleMat);
	projMat.tx += projectionCenter.x;
	projMat.ty += projectionCenter.y;
	return projMat;
}
p._handleTick = function(event) {
	var cameraInstance = exportRoot.___camera___instance;
	if(cameraInstance !== undefined && cameraInstance.pinToObject !== undefined)
	{
		cameraInstance.x = cameraInstance.pinToObject.x + cameraInstance.pinToObject.pinOffsetX;
		cameraInstance.y = cameraInstance.pinToObject.y + cameraInstance.pinToObject.pinOffsetY;
		if(cameraInstance.pinToObject.parent !== undefined && cameraInstance.pinToObject.parent.depth !== undefined)
		cameraInstance.depth = cameraInstance.pinToObject.parent.depth + cameraInstance.pinToObject.pinOffsetZ;
	}
	stage._applyLayerZDepth(exportRoot);
}
p._applyLayerZDepth = function(parent)
{
	var cameraInstance = parent.___camera___instance;
	var focalLength = 528.25;
	var projectionCenter = { 'x' : 0, 'y' : 0};
	if(parent === exportRoot)
	{
		var stageCenter = { 'x' : lib.properties.width/2, 'y' : lib.properties.height/2 };
		projectionCenter.x = stageCenter.x;
		projectionCenter.y = stageCenter.y;
	}
	for(child in parent.children)
	{
		var layerObj = parent.children[child];
		if(layerObj == cameraInstance)
			continue;
		stage._applyLayerZDepth(layerObj, cameraInstance);
		if(layerObj.layerDepth === undefined)
			continue;
		if(layerObj.currentFrame != layerObj.parent.currentFrame)
		{
			layerObj.gotoAndPlay(layerObj.parent.currentFrame);
		}
		var matToApply = new createjs.Matrix2D;
		var cameraMat = new createjs.Matrix2D;
		var totalDepth = layerObj.layerDepth ? layerObj.layerDepth : 0;
		var cameraDepth = 0;
		if(cameraInstance && !layerObj.isAttachedToCamera)
		{
			var mat = cameraInstance.getMatrix();
			mat.tx -= projectionCenter.x;
			mat.ty -= projectionCenter.y;
			cameraMat = mat.invert();
			cameraMat.prependTransform(projectionCenter.x, projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
			cameraMat.appendTransform(-projectionCenter.x, -projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
			if(cameraInstance.depth)
				cameraDepth = cameraInstance.depth;
		}
		if(layerObj.depth)
		{
			totalDepth = layerObj.depth;
		}
		//Offset by camera depth
		totalDepth -= cameraDepth;
		if(totalDepth < -focalLength)
		{
			matToApply.a = 0;
			matToApply.d = 0;
		}
		else
		{
			if(layerObj.layerDepth)
			{
				var sizeLockedMat = stage._getProjectionMatrix(parent, layerObj.layerDepth);
				if(sizeLockedMat)
				{
					sizeLockedMat.invert();
					matToApply.prependMatrix(sizeLockedMat);
				}
			}
			matToApply.prependMatrix(cameraMat);
			var projMat = stage._getProjectionMatrix(parent, totalDepth);
			if(projMat)
			{
				matToApply.prependMatrix(projMat);
			}
		}
		layerObj.transformMatrix = matToApply;
	}
}
an.makeResponsive = function(isResp, respDim, isScale, scaleType, domContainers) {		
	var lastW, lastH, lastS=1;		
	window.addEventListener('resize', resizeCanvas);		
	resizeCanvas();		
	function resizeCanvas() {			
		var w = lib.properties.width, h = lib.properties.height;			
		var iw = window.innerWidth, ih=window.innerHeight;			
		var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;			
		if(isResp) {                
			if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {                    
				sRatio = lastS;                
			}				
			else if(!isScale) {					
				if(iw<w || ih<h)						
					sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==1) {					
				sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==2) {					
				sRatio = Math.max(xRatio, yRatio);				
			}			
		}
		domContainers[0].width = w * pRatio * sRatio;			
		domContainers[0].height = h * pRatio * sRatio;
		domContainers.forEach(function(container) {				
			container.style.width = w * sRatio + 'px';				
			container.style.height = h * sRatio + 'px';			
		});
		stage.scaleX = pRatio*sRatio;			
		stage.scaleY = pRatio*sRatio;
		lastW = iw; lastH = ih; lastS = sRatio;            
		stage.tickOnUpdate = false;            
		stage.update();            
		stage.tickOnUpdate = true;		
	}
}

// Virtual camera API : 

an.VirtualCamera = new function() {
var _camera = new Object();
function VC(timeline) {
	this.timeline = timeline;
	this.camera = timeline.___camera___instance;
	this.centerX = lib.properties.width / 2;
	this.centerY = lib.properties.height / 2;
	this.camAxisX = this.camera.x;
	this.camAxisY = this.camera.y;
	if(timeline.___camera___instance == null || timeline.___camera___instance == undefined ) {
		timeline.___camera___instance = new cjs.MovieClip();
		timeline.___camera___instance.visible = false;
		timeline.___camera___instance.parent = timeline;
		timeline.___camera___instance.setTransform(this.centerX, this.centerY);
	}
	this.camera = timeline.___camera___instance;
}

VC.prototype.moveBy = function(x, y, z) {
z = typeof z !== 'undefined' ? z : 0;
	var position = this.___getCamPosition___();
	var rotAngle = this.getRotation()*Math.PI/180;
	var sinTheta = Math.sin(rotAngle);
	var cosTheta = Math.cos(rotAngle);
	var offX= x*cosTheta + y*sinTheta;
	var offY = y*cosTheta - x*sinTheta;
	this.camAxisX = this.camAxisX - x;
	this.camAxisY = this.camAxisY - y;
	var posX = position.x + offX;
	var posY = position.y + offY;
	this.camera.x = this.centerX - posX;
	this.camera.y = this.centerY - posY;
	this.camera.depth += z;
};

VC.prototype.setPosition = function(x, y, z) {
	z = typeof z !== 'undefined' ? z : 0;

	const MAX_X = 10000;
	const MIN_X = -10000;
	const MAX_Y = 10000;
	const MIN_Y = -10000;
	const MAX_Z = 10000;
	const MIN_Z = -5000;

	if(x > MAX_X)
	  x = MAX_X;
	else if(x < MIN_X)
	  x = MIN_X;
	if(y > MAX_Y)
	  y = MAX_Y;
	else if(y < MIN_Y)
	  y = MIN_Y;
	if(z > MAX_Z)
	  z = MAX_Z;
	else if(z < MIN_Z)
	  z = MIN_Z;

	var rotAngle = this.getRotation()*Math.PI/180;
	var sinTheta = Math.sin(rotAngle);
	var cosTheta = Math.cos(rotAngle);
	var offX= x*cosTheta + y*sinTheta;
	var offY = y*cosTheta - x*sinTheta;
	
	this.camAxisX = this.centerX - x;
	this.camAxisY = this.centerY - y;
	this.camera.x = this.centerX - offX;
	this.camera.y = this.centerY - offY;
	this.camera.depth = z;
};

VC.prototype.getPosition = function() {
	var loc = new Object();
	loc['x'] = this.centerX - this.camAxisX;
	loc['y'] = this.centerY - this.camAxisY;
	loc['z'] = this.camera.depth;
	return loc;
};

VC.prototype.resetPosition = function() {
	this.setPosition(0, 0);
};

VC.prototype.zoomBy = function(zoom) {
	this.setZoom( (this.getZoom() * zoom) / 100);
};

VC.prototype.setZoom = function(zoom) {
	const MAX_zoom = 10000;
	const MIN_zoom = 1;
	if(zoom > MAX_zoom)
	zoom = MAX_zoom;
	else if(zoom < MIN_zoom)
	zoom = MIN_zoom;
	this.camera.scaleX = 100 / zoom;
	this.camera.scaleY = 100 / zoom;
};

VC.prototype.getZoom = function() {
	return 100 / this.camera.scaleX;
};

VC.prototype.resetZoom = function() {
	this.setZoom(100);
};

VC.prototype.rotateBy = function(angle) {
	this.setRotation( this.getRotation() + angle );
};

VC.prototype.setRotation = function(angle) {
	const MAX_angle = 180;
	const MIN_angle = -179;
	if(angle > MAX_angle)
		angle = MAX_angle;
	else if(angle < MIN_angle)
		angle = MIN_angle;
	this.camera.rotation = -angle;
};

VC.prototype.getRotation = function() {
	return -this.camera.rotation;
};

VC.prototype.resetRotation = function() {
	this.setRotation(0);
};

VC.prototype.reset = function() {
	this.resetPosition();
	this.resetZoom();
	this.resetRotation();
	this.unpinCamera();
};
VC.prototype.setZDepth = function(zDepth) {
	const MAX_zDepth = 10000;
	const MIN_zDepth = -5000;
	if(zDepth > MAX_zDepth)
		zDepth = MAX_zDepth;
	else if(zDepth < MIN_zDepth)
		zDepth = MIN_zDepth;
	this.camera.depth = zDepth;
}
VC.prototype.getZDepth = function() {
	return this.camera.depth;
}
VC.prototype.resetZDepth = function() {
	this.camera.depth = 0;
}

VC.prototype.pinCameraToObject = function(obj, offsetX, offsetY, offsetZ) {

	offsetX = typeof offsetX !== 'undefined' ? offsetX : 0;

	offsetY = typeof offsetY !== 'undefined' ? offsetY : 0;

	offsetZ = typeof offsetZ !== 'undefined' ? offsetZ : 0;
	if(obj === undefined)
		return;
	this.camera.pinToObject = obj;
	this.camera.pinToObject.pinOffsetX = offsetX;
	this.camera.pinToObject.pinOffsetY = offsetY;
	this.camera.pinToObject.pinOffsetZ = offsetZ;
};

VC.prototype.setPinOffset = function(offsetX, offsetY, offsetZ) {
	if(this.camera.pinToObject != undefined) {
	this.camera.pinToObject.pinOffsetX = offsetX;
	this.camera.pinToObject.pinOffsetY = offsetY;
	this.camera.pinToObject.pinOffsetZ = offsetZ;
	}
};

VC.prototype.unpinCamera = function() {
	this.camera.pinToObject = undefined;
};
VC.prototype.___getCamPosition___ = function() {
	var loc = new Object();
	loc['x'] = this.centerX - this.camera.x;
	loc['y'] = this.centerY - this.camera.y;
	loc['z'] = this.depth;
	return loc;
};

this.getCamera = function(timeline) {
	timeline = typeof timeline !== 'undefined' ? timeline : null;
	if(timeline === null) timeline = exportRoot;
	if(_camera[timeline] == undefined)
	_camera[timeline] = new VC(timeline);
	return _camera[timeline];
}

this.getCameraAsMovieClip = function(timeline) {
	timeline = typeof timeline !== 'undefined' ? timeline : null;
	if(timeline === null) timeline = exportRoot;
	return this.getCamera(timeline).camera;
}
}


// Layer depth API : 

an.Layer = new function() {
	this.getLayerZDepth = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth; else 0;";
		return eval(script);
	}
	this.setLayerZDepth = function(timeline, layerName, zDepth)
	{
		const MAX_zDepth = 10000;
		const MIN_zDepth = -5000;
		if(zDepth > MAX_zDepth)
			zDepth = MAX_zDepth;
		else if(zDepth < MIN_zDepth)
			zDepth = MIN_zDepth;
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth = " + zDepth + ";";
		eval(script);
	}
	this.removeLayer = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline.removeChild(timeline." + layerName + ");";
		eval(script);
	}
	this.addNewLayer = function(timeline, layerName, zDepth)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		zDepth = typeof zDepth !== 'undefined' ? zDepth : 0;
		var layer = new createjs.MovieClip();
		layer.name = layerName;
		layer.depth = zDepth;
		layer.layerIndex = 0;
		timeline.addChild(layer);
	}
}
an.handleSoundStreamOnTick = function(event) {
	if(!event.paused){
		var stageChild = stage.getChildAt(0);
		if(!stageChild.paused || stageChild.ignorePause){
			stageChild.syncStreamSounds();
		}
	}
}
an.handleFilterCache = function(event) {
	if(!event.paused){
		var target = event.target;
		if(target){
			if(target.filterCacheList){
				for(var index = 0; index < target.filterCacheList.length ; index++){
					var cacheInst = target.filterCacheList[index];
					if((cacheInst.startFrame <= target.currentFrame) && (target.currentFrame <= cacheInst.endFrame)){
						cacheInst.instance.cache(cacheInst.x, cacheInst.y, cacheInst.w, cacheInst.h);
					}
				}
			}
		}
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;