(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [
		{name:"nekocase_atlas_1", frames: [[5593,2152,1000,753],[3208,2932,500,253],[3275,0,1500,1357],[7781,552,400,353],[0,2950,1026,487],[6010,2971,1000,468],[902,3490,500,405],[2006,2932,1200,450],[1004,2135,300,164],[6279,1118,1000,1032],[1004,2330,1000,618],[0,2135,1002,773],[1404,3490,500,404],[3004,1696,200,233],[3004,1377,200,317],[3004,2361,1000,569],[1502,0,1771,1375],[2608,2544,300,353],[4210,3271,700,494],[7012,3426,800,518],[1930,3384,800,534],[1028,2950,900,538],[4277,2074,1314,627],[4277,1936,346,90],[7597,2225,550,544],[2732,3384,300,306],[2006,2330,300,164],[5008,2907,1000,543],[7814,3426,300,297],[7781,907,300,195],[4277,1359,399,298],[4277,1659,400,275],[7012,2971,1000,453],[3208,3271,1000,453],[7281,1118,800,1105],[5008,2703,500,124],[4777,0,1500,1116],[6279,0,1500,1116],[6595,2225,1000,744],[6010,3441,521,548],[4006,2703,1000,566],[3275,1359,1000,1000],[1502,1377,1000,951],[0,0,1500,2133],[2006,2544,600,328],[4777,1118,1500,954],[7781,0,400,550],[4912,3452,1000,272],[0,3439,900,457],[2504,1377,498,1165]]}
];


(lib.AnMovieClip = function(){
	this.actionFrames = [];
	this.ignorePause = false;
	this.gotoAndPlay = function(positionOrLabel){
		cjs.MovieClip.prototype.gotoAndPlay.call(this,positionOrLabel);
	}
	this.play = function(){
		cjs.MovieClip.prototype.play.call(this);
	}
	this.gotoAndStop = function(positionOrLabel){
		cjs.MovieClip.prototype.gotoAndStop.call(this,positionOrLabel);
	}
	this.stop = function(){
		cjs.MovieClip.prototype.stop.call(this);
	}
}).prototype = p = new cjs.MovieClip();
// symbols:



(lib.alley = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.animal_txt_bg = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.animals8 = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.bigdrop = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.city_med_res = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.city_med_respngcopy = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.cloud_1 = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.cloud_2 = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.cloud_3 = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.factory = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.favorite = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.favorite_txt = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.furnace = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(12);
}).prototype = p = new cjs.Sprite();



(lib.furnace_left = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(13);
}).prototype = p = new cjs.Sprite();



(lib.furnace_right = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(14);
}).prototype = p = new cjs.Sprite();



(lib.furnace_wall = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(15);
}).prototype = p = new cjs.Sprite();



(lib.hellon = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(16);
}).prototype = p = new cjs.Sprite();



(lib.hellon_bg = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(17);
}).prototype = p = new cjs.Sprite();



(lib.holdon = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(18);
}).prototype = p = new cjs.Sprite();



(lib.inter3 = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(19);
}).prototype = p = new cjs.Sprite();



(lib.inter_bg1 = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(20);
}).prototype = p = new cjs.Sprite();



(lib.interstitial_2 = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(21);
}).prototype = p = new cjs.Sprite();



(lib.man = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(22);
}).prototype = p = new cjs.Sprite();



(lib.man_title = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(23);
}).prototype = p = new cjs.Sprite();



(lib.moon = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(24);
}).prototype = p = new cjs.Sprite();



(lib.moon_eclipse = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(25);
}).prototype = p = new cjs.Sprite();



(lib.moon_title = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(26);
}).prototype = p = new cjs.Sprite();



(lib.next_time = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(27);
}).prototype = p = new cjs.Sprite();



(lib.next_time_bg = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(28);
}).prototype = p = new cjs.Sprite();



(lib.owl = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(29);
}).prototype = p = new cjs.Sprite();



(lib.polar = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(30);
}).prototype = p = new cjs.Sprite();



(lib.polar_cloud_quote = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(31);
}).prototype = p = new cjs.Sprite();



(lib.raindropswide1 = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(32);
}).prototype = p = new cjs.Sprite();



(lib.raindropswide2 = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(33);
}).prototype = p = new cjs.Sprite();



(lib.redbells = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(34);
}).prototype = p = new cjs.Sprite();



(lib.rubble = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(35);
}).prototype = p = new cjs.Sprite();



(lib.sky_alley = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(36);
}).prototype = p = new cjs.Sprite();



(lib.sky_alley_1 = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(37);
}).prototype = p = new cjs.Sprite();



(lib.sky_alley_gradient = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(38);
}).prototype = p = new cjs.Sprite();



(lib.sky_city_overlay_perspective = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(39);
}).prototype = p = new cjs.Sprite();



(lib.sky_city_perspective = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(40);
}).prototype = p = new cjs.Sprite();



(lib.sky_factory = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(41);
}).prototype = p = new cjs.Sprite();



(lib.sky_furnace = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(42);
}).prototype = p = new cjs.Sprite();



(lib.sky_stars = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(43);
}).prototype = p = new cjs.Sprite();



(lib.star = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(44);
}).prototype = p = new cjs.Sprite();



(lib.stars = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(45);
}).prototype = p = new cjs.Sprite();



(lib.widow = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(46);
}).prototype = p = new cjs.Sprite();



(lib.wild_txt_bg = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(47);
}).prototype = p = new cjs.Sprite();



(lib.wildcreatures = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(48);
}).prototype = p = new cjs.Sprite();



(lib.wolf = function() {
	this.initialize(ss["nekocase_atlas_1"]);
	this.gotoAndStop(49);
}).prototype = p = new cjs.Sprite();



(lib.wolf_1 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.wolf();
	this.instance.setTransform(-249,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-249,0,498,1165);


(lib.wildcreatures_1 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.wildcreatures();
	this.instance.setTransform(-450,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-450,0,900,457);


(lib.widow_1 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.widow();
	this.instance.setTransform(-200,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-200,0,400,550);


(lib.wallright = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.furnace_right();
	this.instance.setTransform(-100,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-100,0,200,317);


(lib.wallleft = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.furnace_left();
	this.instance.setTransform(-100,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-100,0,200,233);


(lib.Tween15 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.man();
	this.instance.setTransform(-378,-181,0.5759,0.5759);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-378,-181,756.8,361.1);


(lib.Tween14 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.sky_city_perspective();
	this.instance.setTransform(-621.2,-430.45,1.2424,1.5211);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-621.2,-430.4,1242.4,860.9);


(lib.Tween13 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.sky_city_perspective();
	this.instance.setTransform(-621.2,-430.45,1.2424,1.5211);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-621.2,-430.4,1242.4,860.9);


(lib.Tween12 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.cloud_1();
	this.instance.setTransform(-250,-202.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-250,-202.5,500,405);


(lib.Tween11 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.cloud_2();
	this.instance.setTransform(-600,-225);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-600,-225,1200,450);


(lib.Tween10 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.cloud_3();
	this.instance.setTransform(-150,-82);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-150,-82,300,164);


(lib.Tween9 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.raindropswide2();
	this.instance.setTransform(-500,-709);

	this.instance_1 = new lib.raindropswide2();
	this.instance_1.setTransform(-500,-227);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-500,-709,1000,935);


(lib.Tween8 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.cloud_3();
	this.instance.setTransform(-351.95,-192.4,2.3463,2.3463);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-351.9,-192.4,703.9,384.8);


(lib.Tween7 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.raindropswide1();
	this.instance.setTransform(-500,-681);

	this.instance_1 = new lib.raindropswide1();
	this.instance_1.setTransform(-500,-227);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-500,-681,1000,907);


(lib.Tween5 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.owl();
	this.instance.setTransform(-150,-97.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-150,-97.5,300,195);


(lib.Tween2 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.animals8();
	this.instance.setTransform(-416.9,-377.15,0.5559,0.5559);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-416.9,-377.1,833.8,754.3);


(lib.Tween1 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.animals8();
	this.instance.setTransform(-416.9,-377.15,0.5559,0.5559);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-416.9,-377.1,833.8,754.3);


(lib.star_txt = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// text
	this.text = new cjs.Text("Read More", "bold 29px 'Inknut Antiqua'", "#552A0F");
	this.text.lineHeight = 31;
	this.text.lineWidth = 292;
	this.text.parent = this;
	this.text.setTransform(-290.2,202.8,0.8971,0.8971);

	this.text_1 = new cjs.Text("Pretium viverra suspendisse potenti nullam ac. Massa ultricies mi quis hendrerit dolor magna.", "24px 'Courier Prime'", "#231F19");
	this.text_1.lineHeight = 27;
	this.text_1.lineWidth = 678;
	this.text_1.parent = this;
	this.text_1.setTransform(-290.2,121.8,0.8971,0.8971);

	this.text_2 = new cjs.Text("“Duis aute irure dolor in repre    \n  naderit in voluptate velit.”", "39px 'Inknut Antiqua'", "#552A0F");
	this.text_2.lineHeight = 57;
	this.text_2.lineWidth = 712;
	this.text_2.parent = this;
	this.text_2.setTransform(-312.7,-43.3,0.8971,0.8971);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.text_2},{t:this.text_1},{t:this.text}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-314.5,-45.1,642.8,339.90000000000003);


(lib.star_text = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// text
	this.text = new cjs.Text("Read More", "bold 29px 'Inknut Antiqua'", "#F0E2C7");
	this.text.lineHeight = 31;
	this.text.lineWidth = 292;
	this.text.parent = this;
	this.text.setTransform(-290.2,202.8,0.8971,0.8971);

	this.text_1 = new cjs.Text("Pretium viverra suspendisse potenti nullam ac. Massa ultricies mi quis hendrerit dolor magna.", "24px 'Courier Prime'", "#FFFFFF");
	this.text_1.lineHeight = 27;
	this.text_1.lineWidth = 678;
	this.text_1.parent = this;
	this.text_1.setTransform(-290.2,121.8,0.8971,0.8971);

	this.text_2 = new cjs.Text("“Duis aute irure dolor in repre    \n  naderit in voluptate velit.”", "39px 'Inknut Antiqua'", "#F1DDB3");
	this.text_2.lineHeight = 57;
	this.text_2.lineWidth = 712;
	this.text_2.parent = this;
	this.text_2.setTransform(-312.7,-43.3,0.8971,0.8971);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.text_2},{t:this.text_1},{t:this.text}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-314.5,-45.1,642.8,339.90000000000003);


(lib.sky_alley_2 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// sky_alley
	this.instance = new lib.sky_alley_1();
	this.instance.setTransform(-750,-558);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-750,-558,1500,1116);


(lib.sarah_text = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// text
	this.text = new cjs.Text("Read More", "bold 29px 'Inknut Antiqua'", "#3B3535");
	this.text.lineHeight = 31;
	this.text.lineWidth = 292;
	this.text.parent = this;
	this.text.setTransform(-290.2,10,0.8971,0.8971);

	this.text_1 = new cjs.Text("“Duis aute irure dolor in repre    \n  naderit in voluptate velit.”", "39px 'Inknut Antiqua'", "#3B3535");
	this.text_1.lineHeight = 57;
	this.text_1.lineWidth = 712;
	this.text_1.parent = this;
	this.text_1.setTransform(-312.7,-99.75,0.8971,0.8971);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.text_1},{t:this.text}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-314.5,-101.5,642.8,203.5);


(lib.polar_txt = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// text
	this.text = new cjs.Text("Read More", "bold 29px 'Inknut Antiqua'", "#F0E0E0");
	this.text.lineHeight = 31;
	this.text.lineWidth = 292;
	this.text.parent = this;
	this.text.setTransform(-290.2,202.8,0.8971,0.8971);

	this.text_1 = new cjs.Text("Pretium viverra suspendisse potenti nullam ac. Massa ultricies mi quis hendrerit dolor magna.", "24px 'Courier Prime'", "#FFFFFF");
	this.text_1.lineHeight = 27;
	this.text_1.lineWidth = 678;
	this.text_1.parent = this;
	this.text_1.setTransform(-290.2,121.8,0.8971,0.8971);

	this.text_2 = new cjs.Text("“Duis aute irure dolor in repre    \n  naderit in voluptate velit.”", "39px 'Inknut Antiqua'", "#E2CAC3");
	this.text_2.lineHeight = 57;
	this.text_2.lineWidth = 712;
	this.text_2.parent = this;
	this.text_2.setTransform(-312.7,-43.3,0.8971,0.8971);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.text_2},{t:this.text_1},{t:this.text}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-314.5,-45.1,642.8,339.90000000000003);


(lib.owl_text = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// text
	this.text = new cjs.Text("Read More", "bold 29px 'Inknut Antiqua'", "#933D00");
	this.text.lineHeight = 31;
	this.text.lineWidth = 292;
	this.text.parent = this;
	this.text.setTransform(-290.2,202.8,0.8971,0.8971);

	this.text_1 = new cjs.Text("Pretium viverra suspendisse potenti nullam ac. Massa ultricies mi quis hendrerit dolor magna.", "24px 'Courier Prime'", "#231F19");
	this.text_1.lineHeight = 27;
	this.text_1.lineWidth = 678;
	this.text_1.parent = this;
	this.text_1.setTransform(-290.2,121.8,0.8971,0.8971);

	this.text_2 = new cjs.Text("“Duis aute irure dolor in repre    \n  naderit in voluptate velit.”", "39px 'Inknut Antiqua'", "#933D00");
	this.text_2.lineHeight = 57;
	this.text_2.lineWidth = 712;
	this.text_2.parent = this;
	this.text_2.setTransform(-312.7,-43.3,0.8971,0.8971);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.text_2},{t:this.text_1},{t:this.text}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-314.5,-45.1,642.8,339.90000000000003);


(lib.next_txt = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// text
	this.text = new cjs.Text("Read More", "bold 29px 'Inknut Antiqua'", "#F0E2C7");
	this.text.lineHeight = 31;
	this.text.lineWidth = 298;
	this.text.parent = this;
	this.text.setTransform(-318.55,409.4968,1.2076,1.2076);

	this.text_1 = new cjs.Text("Pretium viverra suspendisse potenti nullam ac. Massa ultricies mi quis hendrerit dolor magna.", "24px 'Courier Prime'", "#FFFFFF");
	this.text_1.lineHeight = 27;
	this.text_1.lineWidth = 418;
	this.text_1.parent = this;
	this.text_1.setTransform(-318.55,273.2781,1.2076,1.2076);

	this.text_2 = new cjs.Text("“Duis aute irure dolor in repre    \n naderit in voluptate velit.”", "39px 'Inknut Antiqua'", "#F1DDB3");
	this.text_2.lineHeight = 57;
	this.text_2.lineWidth = 416;
	this.text_2.parent = this;
	this.text_2.setTransform(-318.55,-39.25,1.2076,1.2076);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.text_2},{t:this.text_1},{t:this.text}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-320.9,-41.6,509.9,543.9);


(lib.moon_txt = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// text
	this.text = new cjs.Text("Read More", "bold 29px 'Inknut Antiqua'", "#F0E2C7");
	this.text.lineHeight = 31;
	this.text.lineWidth = 292;
	this.text.parent = this;
	this.text.setTransform(-290.2,202.8,0.8971,0.8971);

	this.text_1 = new cjs.Text("Pretium viverra suspendisse potenti nullam ac. Massa ultricies mi quis hendrerit dolor magna.", "24px 'Courier Prime'", "#FFFFFF");
	this.text_1.lineHeight = 27;
	this.text_1.lineWidth = 678;
	this.text_1.parent = this;
	this.text_1.setTransform(-290.2,121.8,0.8971,0.8971);

	this.text_2 = new cjs.Text("“Duis aute irure dolor in repre    \n  naderit in voluptate velit.”", "39px 'Inknut Antiqua'", "#F1DDB3");
	this.text_2.lineHeight = 57;
	this.text_2.lineWidth = 712;
	this.text_2.parent = this;
	this.text_2.setTransform(-312.7,-43.3,0.8971,0.8971);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.text_2},{t:this.text_1},{t:this.text}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-314.5,-45.1,642.8,339.90000000000003);


(lib.inter_txt1 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// text
	this.text = new cjs.Text("Vulputate mi sit amet mauris commodo quis imperdiet. Donec adipiscing tristique risus nec feugiat in. Tempus egestas sed sed risus pretium. Neque laoreet suspendisse interdum consectetur.\n\nScelerisque eu ultrices vitae auctor eu augue ut lectus arcu. Ultrices tincidunt arcu non sodales neque sodales ut etiam sit. Nunc eget lorem dolor sed viverra. Leo a diam sollicitudin tempor id eu nisl nunc.", "28px 'Courier Prime'", "#231F19");
	this.text.lineHeight = 31;
	this.text.lineWidth = 526;
	this.text.parent = this;
	this.text.setTransform(25.65,-47.7,0.5553,0.5553);

	this.text_1 = new cjs.Text("X Close", "bold 29px 'Inknut Antiqua'", "#552A0F");
	this.text_1.lineHeight = 31;
	this.text_1.lineWidth = 178;
	this.text_1.parent = this;
	this.text_1.setTransform(222.6,-104.4,0.5553,0.5553);

	this.text_2 = new cjs.Text("Vulputate mi sit amet mauris commodo quis imperdiet. Donec adipiscing tristique risus nec feugiat in. Tempus egestas sed sed risus pretium. Neque laoreet suspendisse interdum consectetur.\n\nScelerisque eu ultrices vitae auctor eu augue ut lectus arcu. Ultrices tincidunt arcu non sodales neque sodales ut etiam sit. Nunc eget lorem dolor sed viverra. Leo a diam sollicitudin tempor id eu nisl nunc.", "28px 'Courier Prime'", "#231F19");
	this.text_2.lineHeight = 31;
	this.text_2.lineWidth = 526;
	this.text_2.parent = this;
	this.text_2.setTransform(-291.55,64.15,0.5553,0.5553);

	this.text_3 = new cjs.Text("“Aliquet sagittis egestas     \n  fringilla consectetur \n  purus diam vulputate.”", "36px 'Inknut Antiqua'", "#552A0F");
	this.text_3.lineHeight = 49;
	this.text_3.lineWidth = 548;
	this.text_3.parent = this;
	this.text_3.setTransform(-303.85,-66.2,0.5553,0.5553);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.text_3},{t:this.text_2},{t:this.text_1},{t:this.text}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-304.9,-105.5,627.5,460);


(lib.holdon_txt = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// text
	this.text = new cjs.Text("Read More", "bold 29px 'Inknut Antiqua'", "#F0E0E0");
	this.text.lineHeight = 31;
	this.text.lineWidth = 292;
	this.text.parent = this;
	this.text.setTransform(-290.2,202.8,0.8971,0.8971);

	this.text_1 = new cjs.Text("Pretium viverra suspendisse potenti nullam ac. Massa ultricies mi quis hendrerit dolor magna.", "24px 'Courier Prime'", "#FFFFFF");
	this.text_1.lineHeight = 27;
	this.text_1.lineWidth = 678;
	this.text_1.parent = this;
	this.text_1.setTransform(-290.2,121.8,0.8971,0.8971);

	this.text_2 = new cjs.Text("“Duis aute irure dolor in repre    \n  naderit in voluptate velit.”", "39px 'Inknut Antiqua'", "#E2CAC3");
	this.text_2.lineHeight = 57;
	this.text_2.lineWidth = 712;
	this.text_2.parent = this;
	this.text_2.setTransform(-312.7,-43.3,0.8971,0.8971);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.text_2},{t:this.text_1},{t:this.text}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-314.5,-45.1,642.8,339.90000000000003);


(lib.hellon_txt = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// text
	this.text = new cjs.Text("Read More", "bold 29px 'Inknut Antiqua'", "#F0E2C7");
	this.text.lineHeight = 31;
	this.text.lineWidth = 298;
	this.text.parent = this;
	this.text.setTransform(106.4,409.4968,1.2076,1.2076);

	this.text_1 = new cjs.Text("Pretium viverra suspendisse potenti nullam ac. Massa ultricies mi quis hendrerit dolor magna.", "24px 'Courier Prime'", "#FFFFFF");
	this.text_1.lineHeight = 27;
	this.text_1.lineWidth = 575;
	this.text_1.parent = this;
	this.text_1.setTransform(-318.55,231.7471,1.2076,1.2076);

	this.text_2 = new cjs.Text("“Duis aute irure dolor in repre naderit in voluptate velit.”", "39px 'Inknut Antiqua'", "#F1DDB3");
	this.text_2.lineHeight = 57;
	this.text_2.lineWidth = 650;
	this.text_2.parent = this;
	this.text_2.setTransform(-318.55,-39.25,1.2076,1.2076);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.text_2},{t:this.text_1},{t:this.text}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-320.9,-41.6,790,543.9);


(lib.fav_txt = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// text
	this.text = new cjs.Text("Read More", "bold 29px 'Inknut Antiqua'", "#F0E2C7");
	this.text.lineHeight = 31;
	this.text.lineWidth = 298;
	this.text.parent = this;
	this.text.setTransform(106.4,409.4968,1.2076,1.2076);

	this.text_1 = new cjs.Text("Pretium viverra suspendisse potenti nullam ac. Massa ultricies mi quis hendrerit dolor magna.", "24px 'Courier Prime'", "#FFFFFF");
	this.text_1.lineHeight = 27;
	this.text_1.lineWidth = 575;
	this.text_1.parent = this;
	this.text_1.setTransform(-318.55,231.7471,1.2076,1.2076);

	this.text_2 = new cjs.Text("“Duis aute irure dolor in repre naderit in voluptate velit.”", "39px 'Inknut Antiqua'", "#F1DDB3");
	this.text_2.lineHeight = 57;
	this.text_2.lineWidth = 650;
	this.text_2.parent = this;
	this.text_2.setTransform(-318.55,-39.25,1.2076,1.2076);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.text_2},{t:this.text_1},{t:this.text}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-320.9,-41.6,790,543.9);


(lib.creatures_txt = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// text
	this.text = new cjs.Text("Read More", "bold 29px 'Inknut Antiqua'", "#E6E0E8");
	this.text.lineHeight = 31;
	this.text.lineWidth = 292;
	this.text.parent = this;
	this.text.setTransform(-290.2,80.75,0.8971,0.8971);

	this.text_1 = new cjs.Text("“Duis aute irure dolor in repre    \n  naderit in voluptate velit.”", "39px 'Inknut Antiqua'", "#D0C6D1");
	this.text_1.lineHeight = 57;
	this.text_1.lineWidth = 712;
	this.text_1.parent = this;
	this.text_1.setTransform(-312.7,-43.3,0.8971,0.8971);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.text_1},{t:this.text}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-314.5,-45.1,642.8,217.9);


(lib.city_sky = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// sky_city_perspective_jpg
	this.instance = new lib.sky_city_perspective();
	this.instance.setTransform(-800,-453,1.6066,1.6173);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-800,-453,1606.6,915.4);


(lib.animal_text = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// text
	this.text = new cjs.Text("Read More", "bold 29px 'Inknut Antiqua'", "#231F19");
	this.text.lineHeight = 31;
	this.text.lineWidth = 292;
	this.text.parent = this;
	this.text.setTransform(-290.2,186.8,0.8971,0.8971);

	this.text_1 = new cjs.Text("Pretium viverra suspendisse potenti nullam ac. Massa ultricies mi quis hendrerit dolor magna.", "24px 'Courier Prime'", "#231F19");
	this.text_1.lineHeight = 27;
	this.text_1.lineWidth = 678;
	this.text_1.parent = this;
	this.text_1.setTransform(-290.2,121.8,0.8971,0.8971);

	this.text_2 = new cjs.Text("“Duis aute irure dolor in repre    \n  naderit in voluptate velit.”", "39px 'Inknut Antiqua'", "#231F19");
	this.text_2.lineHeight = 57;
	this.text_2.lineWidth = 715;
	this.text_2.parent = this;
	this.text_2.setTransform(-312.7,-9.5,0.8971,0.8971);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.text_2},{t:this.text_1},{t:this.text}]}).wait(1));

	// text_bg
	this.instance = new lib.animal_txt_bg();
	this.instance.setTransform(-435,-197,1.6683,2.5637);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-435,-197,834.2,648.6);


(lib.alley_1 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.alley();
	this.instance.setTransform(-681,-414,1.2401,1.2401);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-681,-414,1240.1,933.8);


(lib.Tween25 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.wild_txt_bg();
	this.instance.setTransform(-525,-142.8,1.05,1.05);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-525,-142.8,1050,285.6);


(lib.Tween24 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.next_time_bg();
	this.instance.setTransform(-243.6,-241.15,1.624,1.624);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-243.6,-241.1,487.2,482.29999999999995);


(lib.Tween23 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.wild_txt_bg();
	this.instance.setTransform(-525,-142.8,1.05,1.05);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-525,-142.8,1050,285.6);


(lib.Symbol3 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.inter_bg1();
	this.instance.setTransform(-400,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-400,0,800,534);


(lib.sun = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["#FF9966","rgba(255,255,0,0.588)","rgba(255,255,204,0)"],[0,0.498,1],-5.2,0.1,0,-5.2,0.1,155.8).s().p("Ax1RCQnDnEAAp9QAAp9HDnEQHDnDKFgBQKEgBIZGyQIYGxhcKRQhcKQnEHDQnDHDp9AAQp+AAnDnDg");
	this.shape.setTransform(0.1483,-5.5503);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-159.2,-159.6,318.7,308.2);


(lib.stars_1 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.stars();
	this.instance.setTransform(-750,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-750,0,1500,954);


(lib.starry_sky = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.sky_stars();
	this.instance.setTransform(-750,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-750,0,1500,2133);


(lib.star_1 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.star();
	this.instance.setTransform(-300,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-300,0,600,328);


(lib.star_bg = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["#000000","rgba(0,0,103,0)"],[0,0.718],0,0,0,0,0,164.9).s().p("Ax1RCQnDnEAAp9QAAp9HDnEQHDnDKFgBQKEgBIZGyQIYGxhcKRQhcKQnEHDQnDHDp9AAQp+AAnDnDg");
	this.shape.setTransform(0.1483,-5.5503);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-159.2,-159.6,318.7,308.2);


(lib.smog = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.sky_city_overlay_perspective();
	this.instance.setTransform(-527.3,0,2.0243,1.4296);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-527.3,0,1054.6999999999998,783.5);


(lib.skyalley = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.sky_alley();
	this.instance.setTransform(-975.85,0,1.3011,1.3011);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-975.8,0,1951.6999999999998,1452.1);


(lib.sky_furnace_1 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.sky_furnace();
	this.instance.setTransform(-525,0,1.05,1.05);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-525,0,1050,998.6);


(lib.sky_factory_1 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.sky_factory();
	this.instance.setTransform(-500,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-500,0,1000,1000);


(lib.sky_alley_gradient_1 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.sky_alley_gradient();
	this.instance.setTransform(-552.95,0,1.1059,1.1059);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-552.9,0,1105.9,822.8);


(lib.rubble_1 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.rubble();
	this.instance.setTransform(-250,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-250,0,500,124);


(lib.polar_1 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.polar();
	this.instance.setTransform(-199.5,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-199.5,0,399,298);


(lib.owl_text_bg = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.animal_txt_bg();
	this.instance.setTransform(-417.05,0,1.6683,2.5637);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-417,0,834.1,648.6);


(lib.next_time_quote = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.next_time_bg();
	this.instance.setTransform(-243.6,-241.15,1.624,1.624);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-243.6,-241.1,487.2,482.29999999999995);


(lib.next_time_1 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.next_time();
	this.instance.setTransform(-798,-86,1.6396,1.6396);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-798,-86,1639.6,890.3);


(lib.moon_title_1 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.moon_title();
	this.instance.setTransform(-150,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-150,0,300,164);


(lib.moon_1 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.moon();
	this.instance.setTransform(-275,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-275,0,550,544);


(lib.man_title_1 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.man_title();
	this.instance.setTransform(-109.75,0,0.6344,0.6344);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-109.7,0,219.5,57.1);


(lib.holdon_cloud = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.polar_cloud_quote();
	this.instance.setTransform(-200,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-200,0,400,275);


(lib.holdon_1 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.holdon();
	this.instance.setTransform(-350,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-350,0,700,494);


(lib.hellon_txt_1 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.hellon_bg();
	this.instance.setTransform(-224.4,381.4,1.2714,1.2714,-90);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-224.4,0,448.8,381.4);


(lib.hellon_1 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.hellon();
	this.instance.setTransform(-392,-265,0.718,0.718);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-392,-265,1271.6,987.3);


(lib.furnace_wall_1 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.furnace_wall();
	this.instance.setTransform(-500,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-500,0,1000,569);


(lib.furnace_1 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.furnace();
	this.instance.setTransform(-250,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-250,0,500,404);


(lib.favorit = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.favorite();
	this.instance.setTransform(-500,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-500,0,1000,618);


(lib.fav_txt_1 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.favorite_txt();
	this.instance.setTransform(-501,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-501,0,1002,773);


(lib.factory_1 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.factory();
	this.instance.setTransform(-525,0,1.05,1.05);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-525,0,1050,1083.6);


(lib.eclipse = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.moon_eclipse();
	this.instance.setTransform(-228.55,0,1.5239,1.5239);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-228.5,0,457.1,466.3);


(lib.city_blur = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.city_med_res();
	this.instance.setTransform(-542,-14,1.0858,1.1013);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-542,-14,1085.8,515.4);


(lib.city = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.city_med_respngcopy();
	this.instance.setTransform(-500,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-500,0,1000,468);


(lib.bigdrop_1 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.bigdrop();
	this.instance.setTransform(-200,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-200,0,400,353);


(lib.bells = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.redbells();
	this.instance.setTransform(-400,0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-400,0,800,1105);


(lib.Tween21 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_2
	this.instance = new lib.inter_txt1("synched",0);
	this.instance.setTransform(-31.15,-55.05,0.8396,0.8396);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer_1
	this.instance_1 = new lib.Symbol3("synched",0);
	this.instance_1.setTransform(0,0,1,1,0,0,0,0,267);
	var instance_1Filter_1 = new cjs.ColorFilter(0.79,0.79,0.79,1,53.55,53.55,53.55,0);
	this.instance_1.filters = [instance_1Filter_1];
	this.instance_1.cache(-402,-2,804,538);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));
	this.timeline.addTween(cjs.Tween.get(instance_1Filter_1).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-400,-267,800,534);


(lib.owl_1 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.Tween5("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-150,-97.5,300,195);


(lib.inter3_1 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_2
	this.instance = new lib.inter_txt1("synched",0);
	this.instance.setTransform(-31.15,-55.05,0.8396,0.8396);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer_3
	this.instance_1 = new lib.inter3();
	this.instance_1.setTransform(-378,-259);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-378,-259,800,518);


(lib.inter2 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_2
	this.instance = new lib.inter_txt1("synched",0);
	this.instance.setTransform(-31.15,-55.05,0.8396,0.8396);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer_3
	this.instance_1 = new lib.interstitial_2();
	this.instance_1.setTransform(-436,-227,0.9465,0.9465);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-436,-267,851.9,549.3);


(lib.rain = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_15
	this.instance = new lib.Tween9("synched",0);
	this.instance.setTransform(26,244.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(118).to({_off:false},0).to({y:1832},29).to({_off:true},1).wait(5));

	// Layer_14
	this.instance_1 = new lib.Tween7("synched",0);
	this.instance_1.setTransform(0,226.5);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(118).to({_off:false},0).to({y:1677.05},29).to({_off:true},1).wait(5));

	// Layer_13
	this.instance_2 = new lib.Tween9("synched",0);
	this.instance_2.setTransform(26,244.5);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(102).to({_off:false},0).to({y:2105.75},34).to({_off:true},1).wait(16));

	// Layer_12
	this.instance_3 = new lib.Tween7("synched",0);
	this.instance_3.setTransform(0,226.5);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(102).to({_off:false},0).to({y:1927.15},34).to({_off:true},1).wait(16));

	// Layer_11
	this.instance_4 = new lib.Tween9("synched",0);
	this.instance_4.setTransform(26,244.5);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(84).to({_off:false},0).to({y:2037.25},34).to({_off:true},1).wait(34));

	// Layer_10
	this.instance_5 = new lib.Tween7("synched",0);
	this.instance_5.setTransform(0,226.5);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(84).to({_off:false},0).to({y:1906.1},33).to({_off:true},2).wait(34));

	// Layer_9
	this.instance_6 = new lib.Tween9("synched",0);
	this.instance_6.setTransform(26,244.5);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(68).to({_off:false},0).to({y:2105.75},34).to({_off:true},1).wait(50));

	// Layer_8
	this.instance_7 = new lib.Tween7("synched",0);
	this.instance_7.setTransform(0,226.5);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(68).to({_off:false},0).to({y:1927.15},34).to({_off:true},1).wait(50));

	// Layer_7
	this.instance_8 = new lib.Tween7("synched",0);
	this.instance_8.setTransform(0,226.5);
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(50).to({_off:false},0).to({y:1906.1},33).to({_off:true},1).wait(69));

	// Layer_6
	this.instance_9 = new lib.Tween7("synched",0);
	this.instance_9.setTransform(0,226.5);
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(37).to({_off:false},0).to({y:1927.15},34).to({_off:true},1).wait(81));

	// Layer_5
	this.instance_10 = new lib.Tween9("synched",0);
	this.instance_10.setTransform(26,244.5);
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(28).to({_off:false},0).to({y:2105.75},34).to({_off:true},1).wait(90));

	// Layer_4
	this.instance_11 = new lib.Tween9("synched",0);
	this.instance_11.setTransform(26,244.5);
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(13).to({_off:false},0).to({y:2037.25},34).to({_off:true},1).wait(105));

	// Layer_3
	this.instance_12 = new lib.Tween7("synched",0);
	this.instance_12.setTransform(0,226.5);
	this.instance_12._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(13).to({_off:false},0).to({y:1906.1},33).to({_off:true},1).wait(106));

	// Layer_2
	this.instance_13 = new lib.Tween9("synched",0);
	this.instance_13.setTransform(26,244.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_13).to({y:2105.75},34).to({_off:true},1).wait(118));

	// Layer_1
	this.instance_14 = new lib.Tween7("synched",0);
	this.instance_14.setTransform(0,226.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_14).to({y:1927.15},34).to({_off:true},1).wait(118));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-500,-464.5,1026,2796.3);


// stage content:
(lib.wild_creatures = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {halls_start:544,"halls_start":770,star_start:1026};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// bells
	this.instance = new lib.bells("synched",0);
	this.instance.setTransform(512,1384.5,1,1,0,0,0,0,552.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(3897).to({_off:false},0).to({y:283.7},26).wait(137));

	// furnace
	this.instance_1 = new lib.furnace_1("synched",0);
	this.instance_1.setTransform(472.2,1232.2,1.7725,1.7725,0,0,0,0.1,202.1);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(3605).to({_off:false},0).to({y:410.1},28).wait(138).to({startPosition:0},0).to({alpha:0},15).wait(274));

	// widow
	this.instance_2 = new lib.widow_1("synched",0);
	this.instance_2.setTransform(513.6,517.7,0.9206,0.9206,0,0,0,0,275.1);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(3771).to({_off:false},0).to({alpha:1},15).wait(274));

	// wall_right
	this.instance_3 = new lib.wallright("synched",0);
	this.instance_3.setTransform(924,1229.05,1,1,0,0,0,0,158.5);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(3605).to({_off:false},0).to({x:934,y:629.1},28).wait(427));

	// wall_left
	this.instance_4 = new lib.wallleft("synched",0);
	this.instance_4.setTransform(44,1271.05,1,1,0,0,0,0,116.5);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(3605).to({_off:false},0).to({x:86,y:663.1},28).wait(427));

	// wall
	this.instance_5 = new lib.furnace_wall_1("synched",0);
	this.instance_5.setTransform(540,929.5,1,1,0,0,0,0,284.5);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(3566).to({_off:false},0).to({x:508,alpha:1},18).wait(21).to({startPosition:0},0).to({regX:0.1,regY:284.6,scaleX:1.05,scaleY:1.05,x:515.1,y:504.95},28).wait(427));

	// sky_furnace
	this.instance_6 = new lib.sky_furnace_1("synched",0);
	this.instance_6.setTransform(515,495.2,1,1,0,0,0,0,499.2);
	this.instance_6.alpha = 0;
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(3566).to({_off:false},0).to({alpha:1},18).wait(21).to({startPosition:0},0).to({y:304.25},29).wait(426));

	// txt_bg_copy_copy
	this.instance_7 = new lib.inter3_1("synched",0);
	this.instance_7.setTransform(551,1042);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(3451).to({_off:false},0).to({x:527.2,y:359.95},16).wait(73).to({startPosition:0},0).to({x:551,y:-396.6},16).to({_off:true},2).wait(502));

	// creatures_txt
	this.instance_8 = new lib.creatures_txt("synched",0);
	this.instance_8.setTransform(550.25,851.75,1,1,0,0,0,6.9,124.9);
	this.instance_8.alpha = 0;
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(3334).to({_off:false},0).to({y:739.75,alpha:1},15).wait(81).to({startPosition:0},0).to({y:851.75,alpha:0},15).to({_off:true},1).wait(614));

	// creature_txt_bg
	this.instance_9 = new lib.Tween23("synched",0);
	this.instance_9.setTransform(515,756.8);
	this.instance_9.alpha = 0;
	this.instance_9._off = true;

	this.instance_10 = new lib.Tween25("synched",0);
	this.instance_10.setTransform(507,644.8);
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(3302).to({_off:false},0).to({alpha:1},14).to({_off:true,x:507,y:644.8},18).wait(726));
	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(3316).to({_off:false},18).wait(96).to({startPosition:0},0).to({alpha:0},6).to({_off:true},24).wait(600));

	// wildcreatures
	this.instance_11 = new lib.wildcreatures_1("synched",0);
	this.instance_11.setTransform(518,1284.5,1,1,0,0,0,0,228.5);
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(3200).to({_off:false},0).to({y:682.8},21).wait(11).to({startPosition:0},0).to({regY:228.6,scaleX:1.2579,scaleY:1.2579,x:504.05,y:491.65},12).wait(72).to({startPosition:0},0).to({regX:0.1,regY:228.8,scaleX:0.9758,scaleY:0.9758,x:514.7,y:363.8},18).wait(96).to({startPosition:0},0).to({y:-293.2},29).to({_off:true},1).wait(600));

	// factory
	this.instance_12 = new lib.factory_1("synched",0);
	this.instance_12.setTransform(515,1581.8,1,1,0,0,0,0,541.8);
	this.instance_12._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(3110).to({_off:false},0).to({x:525,y:633.55},44).wait(32).to({startPosition:0},0).to({y:369.5},29).wait(17).to({startPosition:0},0).to({regX:0.1,scaleX:1.1181,scaleY:1.1181,x:525.1,y:182.2},12).wait(72).to({startPosition:0},0).to({scaleX:0.9817,scaleY:0.9817,y:54.6},18).wait(96).to({startPosition:0},0).to({x:508.7,y:-530.3},29).to({_off:true},1).wait(600));

	// polar_txt
	this.instance_13 = new lib.polar_txt("synched",0);
	this.instance_13.setTransform(786.3,442.1,0.6233,0.6233,0,0,0,7,124.8);
	this.instance_13.alpha = 0;
	this.instance_13._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(2939).to({_off:false},0).to({x:720.3,alpha:1},24).wait(105).to({startPosition:0},0).to({x:181.3,y:380.15,alpha:0},14).to({_off:true},1).wait(977));

	// polar_txt_bg
	this.instance_14 = new lib.holdon_cloud("synched",0);
	this.instance_14.setTransform(1326.45,422.1,1.5527,1.5527,0,0,0,0.1,137.5);
	this.instance_14._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(2885).to({_off:false},0).to({scaleX:1.9165,scaleY:1.9165,x:713.65},48).wait(135).to({startPosition:0},0).to({x:-492.6,y:269.45},32).to({_off:true},1).wait(959));

	// polar
	this.instance_15 = new lib.polar_1("synched",0);
	this.instance_15.setTransform(1224.5,384,1,1,0,0,0,0,149);
	this.instance_15._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(2849).to({_off:false},0).to({x:199.5},63).wait(137).to({startPosition:0},0).to({x:-92.55},22).to({_off:true},30).wait(959));

	// holdon_txt
	this.instance_16 = new lib.holdon_txt("synched",0);
	this.instance_16.setTransform(394.15,446.1,0.6233,0.6233,0,0,0,7,124.8);
	this.instance_16.alpha = 0;
	this.instance_16._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(2683).to({_off:false},0).to({x:348.15,alpha:1},20).wait(109).to({startPosition:0},0).to({x:-190,alpha:0},32).to({_off:true},1).wait(1215));

	// holdon_txt_bg
	this.instance_17 = new lib.holdon_cloud("synched",0);
	this.instance_17.setTransform(1166.9,598.6,1,1,0,0,0,0,137.5);
	this.instance_17.alpha = 0;
	this.instance_17._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(2498).to({_off:false},0).to({alpha:1},111).to({regX:0.1,scaleX:1.2345,scaleY:1.2345,x:817.4,y:514.6},28).to({scaleX:1.5527,scaleY:1.5527,x:342.8,y:400.55},38).wait(137).to({startPosition:0},0).to({x:-195.35},32).to({_off:true},1).wait(1215));

	// holdon
	this.instance_18 = new lib.holdon_1("synched",0);
	this.instance_18.setTransform(1074.15,367,1,1,0,0,0,0,247);
	this.instance_18.alpha = 0;
	this.instance_18._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(2498).to({_off:false},0).to({alpha:1},69).to({x:690.4,y:133},70).wait(175).to({startPosition:0},0).to({x:-225.85,y:-75.05},32).to({_off:true},1).wait(1215));

	// holdon_sky
	this.instance_19 = new lib.sky_factory_1("synched",0);
	this.instance_19.setTransform(520.3,520.2,1.0404,1.0404,0,0,0,0.1,500);
	this.instance_19.alpha = 0.2617;
	this.instance_19._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_19).wait(2498).to({_off:false},0).to({alpha:1},43).wait(569).to({startPosition:0},0).to({y:267.85},44).wait(412).to({startPosition:0},0).to({alpha:0},18).to({_off:true},1).wait(475));

	// txt_bg_copy
	this.instance_20 = new lib.inter2("synched",0);
	this.instance_20.setTransform(551,1042);
	this.instance_20._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_20).wait(2392).to({_off:false},0).to({x:527.2,y:359.95},16).wait(73).to({startPosition:0},0).to({x:551,y:-396.6},16).to({_off:true},2).wait(1561));

	// next_time_txt
	this.next_text = new lib.next_txt("synched",0);
	this.next_text.name = "next_text";
	this.next_text.setTransform(602,335.9,0.692,0.692,0,0,0,0.2,0.2);
	this.next_text.alpha = 0;
	this.next_text._off = true;

	this.timeline.addTween(cjs.Tween.get(this.next_text).wait(1689).to({_off:false},0).to({alpha:1},16).wait(69).to({startPosition:0},0).to({alpha:0},16).to({_off:true},1).wait(2269));

	// next_time_bg
	this.instance_21 = new lib.next_time_quote("synched",0);
	this.instance_21.setTransform(1200.6,901.15);
	this.instance_21._off = true;

	this.instance_22 = new lib.Tween24("synched",0);
	this.instance_22.setTransform(1200.6,667.6);
	this.instance_22._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_21).wait(1629).to({_off:false},0).to({_off:true,y:667.6},11).wait(2420));
	this.timeline.addTween(cjs.Tween.get(this.instance_22).wait(1629).to({_off:false},11).wait(29).to({startPosition:0},0).to({scaleX:1.3752,scaleY:1.3752,x:537.8,y:458.4},15).wait(120).to({startPosition:0},0).to({regX:-0.2,regY:0.1,scaleX:0.842,scaleY:0.842,x:-91.4,y:605.85},17).wait(250).to({startPosition:0},0).to({x:-194.65},9).to({_off:true},2).wait(1978));

	// next_time
	this.instance_23 = new lib.next_time_1("synched",0);
	this.instance_23.setTransform(544.55,1160.65,0.6823,0.7993,0,0,0,0.1,369.1);
	this.instance_23._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_23).wait(1614).to({_off:false},0).to({scaleX:0.6305,scaleY:0.7386,x:496.4,y:547.5},26).wait(29).to({startPosition:0},0).to({regX:0,regY:368.9,scaleX:0.7907,scaleY:0.9262,x:-243.35,y:364.75},15).wait(120).to({startPosition:0},0).to({x:-1025.95},17).to({_off:true},1).wait(2238));

	// fav_txt
	this.fav_text = new lib.fav_txt("synched",0);
	this.fav_text.name = "fav_text";
	this.fav_text.setTransform(499.85,271.45,0.692,0.692,0,0,0,0.2,0.2);
	this.fav_text.alpha = 0;
	this.fav_text._off = true;

	this.timeline.addTween(cjs.Tween.get(this.fav_text).wait(1980).to({_off:false},0).to({y:184.95,alpha:1},10).wait(48).to({startPosition:0},0).to({y:271.45,alpha:0},10).to({_off:true},1).wait(2011));

	// fav_txt_bg
	this.instance_24 = new lib.fav_txt_1("synched",0);
	this.instance_24.setTransform(1031.5,768,0.1513,0.1513,0,0,0,0,386.4);
	this.instance_24._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_24).wait(1820).to({_off:false},0).wait(134).to({startPosition:0},0).to({regX:0.1,regY:386.8,scaleX:0.9535,scaleY:0.9535,rotation:41.1973,x:534.2,y:288.5},20,cjs.Ease.bounceIn).wait(73).to({startPosition:0},0).to({regX:0,regY:386.4,scaleX:0.1513,scaleY:0.1513,rotation:0,x:1031.5,y:768},20).to({_off:true},1).wait(1992));

	// rubble
	this.instance_25 = new lib.rubble_1("synched",0);
	this.instance_25.setTransform(1172.75,686.35,1.317,1.317,0,0,0,0,62);
	this.instance_25._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_25).wait(2063).to({_off:false},0).wait(8).to({startPosition:0},0).to({regX:0.1,regY:62.1,scaleX:1.8871,scaleY:1.8871,x:687.85,y:685.2},14).to({regX:0,regY:62,scaleX:1.317,scaleY:1.317,x:62.85,y:686.35},18).to({_off:true},243).wait(1714));

	// hellon
	this.instance_26 = new lib.hellon_1("synched",0);
	this.instance_26.setTransform(1386.45,546.7,1,1,0,0,0,0,364.5);
	this.instance_26._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_26).wait(2071).to({_off:false},0).to({regX:0.1,scaleX:1.0535,scaleY:1.0535,x:949.5,y:614.45},14).to({regY:364.6,scaleX:1.0505,scaleY:1.0505,x:916.5,y:605},1).to({regX:0,regY:364.5,scaleX:1,scaleY:1,x:356.1,y:443.35},17).to({regX:0.2,regY:364.7,scaleX:0.8307,scaleY:0.8307,x:307.3,y:482.45},25).wait(216).to({startPosition:0},0).to({x:325.8,y:1286.7,alpha:0.2617},20).to({_off:true},1).wait(1695));

	// hellon_txt
	this.hellon_text = new lib.hellon_txt("synched",0);
	this.hellon_text.name = "hellon_text";
	this.hellon_text.setTransform(287.1,73.05,0.3376,0.3376,0,0,0,0.3,0.3);
	this.hellon_text.alpha = 0;
	this.hellon_text._off = true;

	this.timeline.addTween(cjs.Tween.get(this.hellon_text).wait(2179).to({_off:false},0).to({y:26.95,alpha:1},10).wait(105).to({startPosition:0},0).to({y:73.05,alpha:0},10).to({_off:true},52).wait(1704));

	// hellon_txt_bg
	this.instance_27 = new lib.hellon_txt_1("synched",0);
	this.instance_27.setTransform(336.4,149.7,1,1,0,0,0,0,190.7);
	this.instance_27.alpha = 0;
	this.instance_27._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_27).wait(2152).to({_off:false},0).to({scaleX:1.0947,scaleY:1.0947,x:321.95,y:56.05,alpha:1},24).wait(123).to({startPosition:0},0).to({scaleX:1,scaleY:1,x:336.4,y:149.7,alpha:0},11).to({_off:true},35).wait(1715));

	// favorit
	this.instance_28 = new lib.favorit("synched",0);
	this.instance_28.setTransform(1640.3,629.45,1,1,0,0,0,0,309);
	this.instance_28._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_28).wait(1675).to({_off:false},0).to({x:1340,y:543},9).wait(120).to({scaleX:1.2258,scaleY:1.2258,x:1409.7,y:607.85},0).to({regY:309.1,scaleX:1.0387,scaleY:1.0387,x:512.15,y:447.15},17).wait(250).to({startPosition:0},0).to({regX:0.1,scaleX:1.0943,scaleY:1.0943,x:27.9,y:485.75},14).to({regX:0,scaleX:1.0387,scaleY:1.0387,x:-480.9,y:379.8},18).to({_off:true},1).wait(1956));

	// moon_txt
	this.moon_text = new lib.moon_txt("synched",0);
	this.moon_text.name = "moon_text";
	this.moon_text.setTransform(690,142.15,0.4636,0.4636,0,0,0,0.2,0.2);
	this.moon_text.alpha = 0;
	this.moon_text._off = true;

	this.timeline.addTween(cjs.Tween.get(this.moon_text).wait(1483).to({_off:false},0).to({alpha:1},13).wait(85).to({startPosition:0},0).to({alpha:0},13).wait(1).to({x:706,y:201.55},0).to({_off:true},2).wait(2463));

	// eclipse
	this.instance_29 = new lib.eclipse("synched",0);
	this.instance_29.setTransform(679.5,195.05,0.8097,0.8097,0,0,0,0.1,233.3);
	this.instance_29.alpha = 0;
	this.instance_29._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_29).wait(1461).to({_off:false},0).to({alpha:1},18).wait(114).to({startPosition:0},0).to({alpha:0},18).to({_off:true},1).wait(2448));

	// moon_title
	this.instance_30 = new lib.moon_title_1("synched",0);
	this.instance_30.setTransform(253.7,386.65,1,1,-9.0239,0,0,-0.1,81.3);
	this.instance_30.alpha = 0;
	this.instance_30._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_30).wait(1395).to({_off:false},0).wait(1).to({regX:0,regY:82,rotation:-8.1216,x:260.9,y:386.35,alpha:0.1},0).wait(1).to({rotation:-7.2192,x:267.9,y:385.4,alpha:0.2},0).wait(1).to({rotation:-6.3168,x:274.9,y:384.4,alpha:0.3},0).wait(1).to({rotation:-5.4144,x:281.95,y:383.45,alpha:0.4},0).wait(1).to({rotation:-4.512,x:288.95,y:382.5,alpha:0.5},0).wait(1).to({rotation:-3.6096,x:295.95,y:381.55,alpha:0.6},0).wait(1).to({rotation:-2.7072,x:302.95,y:380.55,alpha:0.7},0).wait(1).to({rotation:-1.8048,x:310,y:379.6,alpha:0.8},0).wait(1).to({rotation:-0.9024,x:317,y:378.65,alpha:0.9},0).wait(1).to({rotation:0,x:324.05,y:377.7,alpha:1},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({y:346.45},0).wait(1).to({y:315.2},0).wait(1).to({x:324.1,y:283.95},0).wait(1).to({y:252.7},0).wait(1).to({x:324.15,y:221.45},0).wait(1).to({y:190.2},0).wait(1).to({y:158.95},0).wait(1).to({x:324.2,y:127.7},0).wait(1).to({y:96.45},0).wait(1).to({x:324.25,y:65.25},0).wait(1).to({y:34},0).wait(1).to({y:2.75},0).wait(1).to({x:324.3,y:-28.5},0).wait(1).to({y:-59.75},0).wait(1).to({x:324.35,y:-91},0).wait(1).to({y:-122.25},0).wait(1).to({y:-153.5},0).wait(1).to({x:324.4,y:-184.75},0).wait(1).to({y:-216},0).wait(1).to({x:324.45,y:-247.25},0).wait(1).to({y:-278.5},0).wait(1).to({x:324.5,y:-309.75},0).to({_off:true},1).wait(2425));

	// moon
	this.instance_31 = new lib.moon_1("synched",0);
	this.instance_31.setTransform(878.25,1054.5,1.3932,1.3932,0,0,0,0.1,272.1);
	this.instance_31._off = true;

	
	var _tweenStr_0 = cjs.Tween.get(this.instance_31).wait(1231).to({_off:false},0).wait(1).to({regX:0,regY:272,scaleX:1.3861,scaleY:1.3861,x:883.25,y:1044.7},0).wait(1).to({scaleX:1.379,scaleY:1.379,x:888.3,y:1035},0).wait(1).to({scaleX:1.3718,scaleY:1.3718,x:893.1,y:1025.2},0).wait(1).to({scaleX:1.3647,scaleY:1.3647,x:897.65,y:1015.3},0).wait(1).to({scaleX:1.3576,scaleY:1.3576,x:902.15,y:1005.3},0).wait(1).to({scaleX:1.3505,scaleY:1.3505,x:906.6,y:995.35},0).wait(1).to({scaleX:1.3433,scaleY:1.3433,x:910.95,y:985.35},0).wait(1).to({scaleX:1.3362,scaleY:1.3362,x:915.2,y:975.25},0).wait(1).to({scaleX:1.3291,scaleY:1.3291,x:919.2,y:965.3},0).wait(1).to({scaleX:1.322,scaleY:1.322,x:922.8,y:954.75},0).wait(1).to({scaleX:1.3148,scaleY:1.3148,x:926.1,y:944.4},0).wait(1).to({scaleX:1.3077,scaleY:1.3077,x:929.35,y:933.95},0).wait(1).to({scaleX:1.3006,scaleY:1.3006,x:932.5,y:923.45},0).wait(1).to({scaleX:1.2935,scaleY:1.2935,x:935.6,y:913},0).wait(1).to({scaleX:1.2863,scaleY:1.2863,x:938.65,y:902.5},0).wait(1).to({scaleX:1.2792,scaleY:1.2792,x:941.6,y:892},0).wait(1).to({scaleX:1.2721,scaleY:1.2721,x:944.5,y:881.45},0).wait(1).to({scaleX:1.265,scaleY:1.265,x:947.3,y:870.9},0).wait(1).to({scaleX:1.2578,scaleY:1.2578,x:950.05,y:860.35},0).wait(1).to({scaleX:1.2507,scaleY:1.2507,x:952.7,y:849.75},0).wait(1).to({scaleX:1.2436,scaleY:1.2436,x:955.3,y:839.1},0).wait(1).to({scaleX:1.2365,scaleY:1.2365,x:957.8,y:828.45},0).wait(1).to({scaleX:1.2293,scaleY:1.2293,x:960.2,y:817.85},0).wait(1).to({scaleX:1.2222,scaleY:1.2222,x:962.45,y:807.15},0).wait(1).to({scaleX:1.2151,scaleY:1.2151,x:964.55,y:796.4},0).wait(1).to({scaleX:1.208,scaleY:1.208,x:966.5,y:785.65},0).wait(1).to({scaleX:1.2008,scaleY:1.2008,x:968.25,y:774.85},0).wait(1).to({scaleX:1.1937,scaleY:1.1937,x:969.7,y:764.05},0).wait(1).to({scaleX:1.1866,scaleY:1.1866,x:970.8,y:753.3},0).wait(1).to({scaleX:1.1795,scaleY:1.1795,x:971.15,y:742.25},0).wait(1).to({scaleX:1.1723,scaleY:1.1723,x:971.1,y:731.3},0).wait(1).to({scaleX:1.1652,scaleY:1.1652,x:970.85,y:720.4},0).wait(1).to({scaleX:1.1581,scaleY:1.1581,x:970.45,y:709.5},0).wait(1).to({scaleX:1.151,scaleY:1.151,x:969.9,y:698.55},0).wait(1).to({scaleX:1.1438,scaleY:1.1438,x:969.2,y:687.65},0).wait(1).to({scaleX:1.1367,scaleY:1.1367,x:968.4,y:676.8},0).wait(1).to({scaleX:1.1296,scaleY:1.1296,x:967.45,y:665.9},0).wait(1).to({scaleX:1.1225,scaleY:1.1225,x:966.45,y:655},0).wait(1).to({scaleX:1.1153,scaleY:1.1153,x:965.25,y:644.15},0).wait(1).to({scaleX:1.1082,scaleY:1.1082,x:963.95,y:633.35},0).wait(1).to({scaleX:1.1011,scaleY:1.1011,x:962.55,y:622.5},0).wait(1).to({scaleX:1.094,scaleY:1.094,x:961,y:611.65},0).wait(1).to({scaleX:1.0868,scaleY:1.0868,x:959.3,y:600.85},0).wait(1).to({scaleX:1.0797,scaleY:1.0797,x:957.45,y:590.15},0).wait(1).to({scaleX:1.0726,scaleY:1.0726,x:955.45,y:579.35},0).wait(1).to({scaleX:1.0655,scaleY:1.0655,x:953.25,y:568.65},0).wait(1).to({scaleX:1.0583,scaleY:1.0583,x:950.85,y:558},0).wait(1).to({scaleX:1.0512,scaleY:1.0512,x:948.2,y:547.4},0).wait(1).to({scaleX:1.0441,scaleY:1.0441,x:945.35,y:536.9},0).wait(1).to({scaleX:1.037,scaleY:1.037,x:942.2,y:526.4},0).wait(1).to({scaleX:1.0298,scaleY:1.0298,x:938.65,y:516.05},0).wait(1).to({scaleX:1.0227,scaleY:1.0227,x:934.75,y:505.9},0).wait(1).to({scaleX:1.0156,scaleY:1.0156,x:930.4,y:495.85},0).wait(1).to({scaleX:1.0085,scaleY:1.0085,x:925.5,y:486.05},0).wait(1).to({scaleX:1.0013,scaleY:1.0013,x:920.3,y:476.45},0).wait(1).to({scaleX:0.9942,scaleY:0.9942,x:914.8,y:467},0).wait(1).to({scaleX:0.9871,scaleY:0.9871,x:909.05,y:457.75},0).wait(1).to({scaleX:0.98,scaleY:0.98,x:903.15,y:448.55},0).wait(1).to({scaleX:0.9728,scaleY:0.9728,x:897.05,y:439.45},0).wait(1).to({scaleX:0.9657,scaleY:0.9657,x:890.85,y:430.45},0).wait(1).to({scaleX:0.9586,scaleY:0.9586,x:884.5,y:421.6},0).wait(1).to({scaleX:0.9515,scaleY:0.9515,x:878.1,y:412.75},0).wait(1).to({scaleX:0.9443,scaleY:0.9443,x:871.55,y:404},0).wait(1).to({scaleX:0.9372,scaleY:0.9372,x:864.95,y:395.3},0).wait(1).to({scaleX:0.9301,scaleY:0.9301,x:858.25,y:386.7},0).wait(1).to({x:851.45,y:378.15},0).wait(1).to({x:844.65,y:369.6},0).wait(1).to({x:837.75,y:361.15},0).wait(1).to({x:830.8,y:352.7},0).wait(1).to({x:823.8,y:344.3},0).wait(1).to({x:816.7,y:335.95},0).wait(1).to({x:809.65,y:327.65},0).wait(1).to({x:802.5,y:319.4},0).wait(1).to({scaleX:0.9168,scaleY:0.9168,x:795.3,y:311.2},0).wait(1).to({scaleX:0.9036,scaleY:0.9036,x:788.05,y:302.95},0).wait(1).to({scaleX:0.8903,scaleY:0.8903,x:780.8,y:294.8},0).wait(1).to({scaleX:0.8771,scaleY:0.8771,x:773.45,y:286.7},0).wait(1).to({scaleX:0.8638,scaleY:0.8638,x:766.1,y:278.65},0).wait(1).to({scaleX:0.8506,scaleY:0.8506,x:758.7,y:270.6},0).wait(1).to({scaleX:0.8373,scaleY:0.8373,x:751.25,y:262.6},0).wait(1).to({scaleX:0.8241,scaleY:0.8241,x:743.8,y:254.65},0).wait(1).to({scaleX:0.8108,scaleY:0.8108,x:736.25,y:246.7},0).wait(1).to({scaleX:0.7976,scaleY:0.7976,x:728.7,y:238.85},0).wait(1).to({scaleX:0.7844,scaleY:0.7844,x:721.1,y:231},0).wait(1).to({scaleX:0.7711,scaleY:0.7711,x:713.4,y:223.25},0).wait(1).to({scaleX:0.7579,scaleY:0.7579,x:705.7,y:215.5},0).wait(1).to({scaleX:0.7446,scaleY:0.7446,x:697.9,y:207.85},0).wait(1).to({scaleX:0.7314,scaleY:0.7314,x:690,y:200.35},0).wait(1).to({scaleX:0.7181,scaleY:0.7181,x:681.9,y:193},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({alpha:0.9812},0).wait(1).to({alpha:0.9625},0).wait(1).to({alpha:0.9438},0).wait(1).to({alpha:0.925},0).wait(1).to({alpha:0.9063},0).wait(1).to({alpha:0.8875},0).wait(1).to({alpha:0.8687},0).wait(1).to({alpha:0.85},0).wait(1).to({alpha:0.8313},0).wait(1).to({alpha:0.8125},0).wait(1).to({alpha:0.7937},0).wait(1).to({alpha:0.775},0).wait(1).to({alpha:0.7563},0).wait(1).to({alpha:0.7375},0).wait(1).to({alpha:0.7188},0).wait(1).to({alpha:0.7},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({y:161.75},0).wait(1).to({x:681.95,y:130.5},0).wait(1).to({y:99.25},0).wait(1).to({x:682,y:68},0).wait(1).to({y:36.75},0).wait(1).to({x:682.05,y:5.55},0).wait(1).to({y:-25.75},0).wait(1).to({y:-57},0).wait(1).to({x:682.1,y:-88.25},0).wait(1).to({y:-119.45},0).wait(1).to({x:682.15,y:-150.75},0).wait(1).to({y:-182},0).wait(1).to({y:-213.25},0).wait(1).to({x:682.2,y:-244.5},0).wait(1).to({y:-275.75},0).wait(1).to({x:682.25,y:-307},0).wait(1);
	this.timeline.addTween(_tweenStr_0.to({y:-338.25},0).wait(1).to({y:-369.5},0).wait(1).to({x:682.3,y:-400.75},0).wait(1).to({y:-432},0).wait(1).to({x:682.35,y:-463.25},0).wait(1).to({y:-494.5},0).to({_off:true},1).wait(2425));

	// wolf
	this.instance_32 = new lib.wolf_1("synched",0);
	this.instance_32.setTransform(42.9,1109.65,0.5333,0.5333,0,0,0,0.1,582.5);
	this.instance_32._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_32).wait(1364).to({_off:false},0).to({regY:582.6,x:101.7,y:770.15},28).wait(220).to({startPosition:0},0).to({x:92.9,y:214.5},28).wait(29).to({startPosition:0},0).to({regX:0,regY:582.5,scaleX:0.7334,scaleY:0.7334,x:-985.5,y:-164.7},15).to({_off:true},1).wait(2375));

	// star_txt
	this.star_text = new lib.star_text("synched",0);
	this.star_text.name = "star_text";
	this.star_text.setTransform(316,152.15,0.6521,0.6521,0,0,0,0.1,0.1);
	this.star_text.alpha = 0;
	this.star_text._off = true;

	this.timeline.addTween(cjs.Tween.get(this.star_text).wait(1106).to({_off:false},0).to({x:314.35,y:181.3,alpha:1},11).wait(55).to({startPosition:0},0).to({x:316,y:152.15,alpha:0},10).to({_off:true},1).wait(2877));

	// star_txt_bg
	this.instance_33 = new lib.star_bg("synched",0);
	this.instance_33.setTransform(294.8,296.5,0.1666,0.1666,0,0,0,0,-5.4);
	this.instance_33._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_33).wait(1084).to({_off:false},0).to({regX:0.1,regY:-4.5,scaleX:3.0627,scaleY:3.0627,x:294.95,y:299.05},21).wait(71).to({startPosition:0},0).to({regX:0,regY:-5.4,scaleX:0.1666,scaleY:0.1666,x:294.8,y:296.5,alpha:0},21).to({_off:true},1).wait(2862));

	// star
	this.instance_34 = new lib.star_1("synched",0);
	this.instance_34.setTransform(-300,-118,1,1,0,0,0,0,164);
	this.instance_34._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_34).wait(1055).to({_off:false},0).to({scaleX:1.7076,scaleY:1.7076,x:500.2,y:320.1},29).wait(113).to({startPosition:0},0).to({x:1536.25,y:1060.35},20).to({_off:true},1).wait(2842));

	// stars
	this.instance_35 = new lib.stars_1("synched",0);
	this.instance_35.setTransform(510.95,277.35,0.5333,0.5333,0,0,0,0.1,477.1);
	this.instance_35.alpha = 0;
	this.instance_35._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_35).wait(1025).to({_off:false},0).to({alpha:1},30).wait(557).to({startPosition:0},0).to({x:511.4,y:-410.15},22).wait(1).to({startPosition:0},0).to({_off:true},1).wait(2424));

	// sky_alley
	this.instance_36 = new lib.skyalley("synched",0);
	this.instance_36.setTransform(503.55,380.8,0.5333,0.5333,0,0,0,0.1,726);
	this.instance_36.alpha = 0;
	this.instance_36._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_36).wait(969).to({_off:false},0).to({alpha:1},22).to({regY:725.9,y:359.45,alpha:0},34).wait(1).to({regY:726,x:498.35,y:-259.35},0).to({_off:true},1).wait(3033));

	// starry_sky
	this.instance_37 = new lib.starry_sky("synched",0);
	this.instance_37.setTransform(0.05,0,0.6824,0.6824,0,0,0,-749.9,0);
	this.instance_37._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_37).wait(992).to({_off:false},0).wait(620).to({startPosition:0},0).to({x:0.5,y:-687.5},22).wait(35).to({startPosition:0},0).to({regX:-749.5,regY:-0.2,scaleX:0.8151,scaleY:0.8151,x:0.45,y:-970.8},15).wait(120).to({startPosition:0},0).to({regY:-0.3,scaleX:0.7047,scaleY:0.7047,x:0.35,y:-970.85},16).wait(248).to({startPosition:0},0).to({regX:-749.6,regY:-0.2,scaleX:0.7429,scaleY:0.7429,x:0.25,y:-932.4},18).to({regX:-749.5,regY:-0.4,scaleX:0.7001,scaleY:0.7001,x:0.3,y:-1023.8},42).wait(216).to({startPosition:0},0).to({x:6.5,y:-329.5},20).to({_off:true},210).wait(1486));

	// txt_bg
	this.instance_38 = new lib.Tween21("synched",0);
	this.instance_38.setTransform(511,1042);
	this.instance_38._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_38).wait(863).to({_off:false},0).to({y:330.8},16).wait(70).to({startPosition:0},0).to({y:-343.5},20).to({_off:true},1).wait(3090));
	this.instance_38.addEventListener("tick", AdobeAn.handleFilterCache);

	// bg
	this.instance_39 = new lib.Tween13("synched",0);
	this.instance_39.setTransform(526.2,1198.45);
	this.instance_39._off = true;
	var instance_39Filter_1 = new cjs.ColorFilter(1,1,1,1,0,0,0,0);
	this.instance_39.filters = [instance_39Filter_1];
	this.instance_39.cache(-623,-432,1246,865);

	this.instance_40 = new lib.Tween14("synched",0);
	this.instance_40.setTransform(526.2,397.45);
	this.instance_40._off = true;
	var instance_40Filter_2 = new cjs.ColorFilter(1,1,1,1,0,0,0,0);
	this.instance_40.filters = [instance_40Filter_2];
	this.instance_40.cache(-623,-432,1246,865);

	this.timeline.addTween(cjs.Tween.get(this.instance_39).wait(834).to({_off:false},0).to({y:797.95},10).to({_off:true,y:397.45},10).wait(3206));
	this.timeline.addTween(cjs.Tween.get(this.instance_40).wait(844).to({_off:false},10).to({scaleX:0.8389,scaleY:0.8897,x:510.45,y:384.4},9).to({startPosition:0},16).wait(112).to({startPosition:0},0).to({_off:true},1).wait(3068));
	this.timeline.addTween(cjs.Tween.get(instance_39Filter_1).wait(834).to(new cjs.ColorFilter(0,0,0,1,0,0,0,0), 0).wait(10).to(new cjs.ColorFilter(1,1,1,1,0,0,0,0), 10).wait(3206));
	this.timeline.addTween(cjs.Tween.get(instance_40Filter_2).wait(844).to(new cjs.ColorFilter(1,1,1,1,0,0,0,0), 10).wait(9).to(new cjs.ColorFilter(0.8,0.8,0.8,1,0,0,0,0), 16).wait(3180));

	// sarah_text
	this.sarah_text = new lib.sarah_text("synched",0);
	this.sarah_text.name = "sarah_text";
	this.sarah_text.setTransform(556.1,-90.6,0.336,0.336,0,0,0,0.1,0);
	this.sarah_text.alpha = 0;
	this.sarah_text._off = true;

	this.timeline.addTween(cjs.Tween.get(this.sarah_text).wait(559).to({_off:false},0).to({regY:0.2,scaleX:0.5298,scaleY:0.5298,y:242.4,alpha:1},13).wait(86).to({startPosition:0},0).to({y:977.35},13).to({_off:true},1).wait(3388));

	// sarah_txt_bg
	this.bigdrop = new lib.bigdrop_1("synched",0);
	this.bigdrop.name = "bigdrop";
	this.bigdrop.setTransform(543,-181.5,1,1,0,0,0,0,176.5);
	this.bigdrop.alpha = 0;
	this.bigdrop._off = true;

	this.timeline.addTween(cjs.Tween.get(this.bigdrop).wait(559).to({_off:false},0).to({y:211,alpha:1},13).wait(86).to({startPosition:0},0).to({y:945.95},13).to({_off:true},1).wait(3388));

	// taco_txt
	this.taco_text = new lib.star_txt("synched",0);
	this.taco_text.name = "taco_text";
	this.taco_text.setTransform(157.55,336.4,0.8396,0.8396);
	this.taco_text.alpha = 0;
	this.taco_text._off = true;

	this.timeline.addTween(cjs.Tween.get(this.taco_text).wait(734).to({_off:false},0).to({x:334.1,alpha:1},16).wait(62).to({startPosition:0},0).to({regX:0.1,regY:0.1,x:906.3,y:336.45,alpha:0},11).to({regX:0,regY:0,x:1478.45,y:336.4},11).to({_off:true},1).wait(3225));

	// tacotruck
	this.instance_41 = new lib.Tween15("synched",0);
	this.instance_41.setTransform(-871.7,475.95,2.2403,2.2403,0,0,0,0,0.1);
	this.instance_41._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_41).wait(704).to({_off:false},0).to({x:740.35},30).wait(78).to({startPosition:0},0).to({x:1884.7},22).to({_off:true},1).wait(3225));

	// owl_path
	this.onl = new lib.owl_1("synched",0);
	this.onl.name = "onl";
	this.onl.setTransform(279.55,157.95);
	this.onl._off = true;

	this.timeline.addTween(cjs.Tween.get(this.onl).wait(531).to({_off:false},0).wait(1).to({rotation:11.3652,x:375.1888,y:162.7894},0).wait(1).to({rotation:26.9402,x:467.4651,y:188.6355},0).wait(1).to({rotation:51.3158,x:544.5412,y:244.5433},0).wait(1).to({rotation:65.6022,x:596.4353,y:325.0502},0).wait(1).to({rotation:64.6197,x:639.808,y:410.7523},0).wait(1).to({rotation:54.3651,x:692.0356,y:491.1896},0).wait(1).to({rotation:40.4321,x:760.5129,y:558.1564},0).wait(1).to({rotation:30.6618,x:842.2971,y:608.2102},0).wait(1).to({rotation:23.7993,x:929.6533,y:648.0793},0).wait(1).to({rotation:13.9276,x:1021.8429,y:674.4537},0).wait(1).to({rotation:5.2672,x:1117.2026,y:684.9118},0).wait(1).to({rotation:0.8785,x:1213.2389,y:684.496},0).wait(1).to({rotation:-0.4739,x:1309.15,y:679.6},0).to({_off:true},1).wait(3515));

	// owl
	this.instance_42 = new lib.owl_1("synched",0);
	this.instance_42.setTransform(-186.3,256.65);
	this.instance_42._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_42).wait(453).to({_off:false},0).to({x:279.55,y:157.95},17).to({_off:true},61).wait(3529));

	// owl_text
	this.owl_text = new lib.owl_text("synched",0);
	this.owl_text.name = "owl_text";
	this.owl_text.setTransform(523.2,459.05,0.8396,0.8396);
	this.owl_text.alpha = 0;
	this.owl_text._off = true;

	this.timeline.addTween(cjs.Tween.get(this.owl_text).wait(470).to({_off:false},0).to({y:377.45,alpha:1},13).wait(48).to({startPosition:0},0).to({y:459.05,alpha:0},13).to({_off:true},1).wait(3515));

	// Cloud
	this.instance_43 = new lib.Tween8("synched",0);
	this.instance_43.setTransform(223.95,956.4);
	this.instance_43._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_43).wait(420).to({_off:false},0).to({x:246.55,y:677.95},10).to({x:977.05,y:651.95},27).wait(74).to({startPosition:0},0).to({scaleX:0.836,scaleY:0.836,x:953.4,y:236.1},13).wait(7).to({startPosition:0},0).to({y:-32.35,alpha:0.1484},8).to({_off:true},113).wait(3388));

	// rain
	this.rain = new lib.rain("synched",0);
	this.rain.name = "rain";
	this.rain.setTransform(500,-239.5,1,1,0,0,0,0,226.5);
	this.rain.alpha = 0;
	this.rain._off = true;

	this.timeline.addTween(cjs.Tween.get(this.rain).wait(544).to({_off:false},0).to({alpha:0.6211,startPosition:15},28).wait(112).to({startPosition:127},0).to({alpha:0,startPosition:147},20).to({_off:true},1).wait(3355));

	// man_title
	this.man_title = new lib.man_title_1("synched",0);
	this.man_title.name = "man_title";
	this.man_title.setTransform(360.75,722.65,1.4141,1.4141,0,0,0,0,28.6);
	this.man_title.alpha = 0;
	this.man_title._off = true;

	this.timeline.addTween(cjs.Tween.get(this.man_title).wait(684).to({_off:false},0).to({scaleX:1,scaleY:1,y:722.6,alpha:1},20).wait(130).to({startPosition:0},0).to({y:-78.15},20).to({_off:true},1).wait(3205));

	// Alley
	this.alley = new lib.alley_1("synched",0);
	this.alley.name = "alley";
	this.alley.setTransform(640.75,1177.6,1.2161,1.2161);
	this.alley._off = true;

	this.timeline.addTween(cjs.Tween.get(this.alley).wait(544).to({_off:false},0).to({scaleX:0.8879,scaleY:0.8879,x:573.3,y:811.2},7).to({regX:0.2,regY:0.2,scaleX:0.8423,scaleY:0.8423,x:567.05,y:330.3},8).wait(253).to({startPosition:0},0).wait(22).to({startPosition:0},0).to({y:-470.45},20).to({_off:true},1).wait(3205));

	// cloud3
	this.instance_44 = new lib.Tween10("synched",0);
	this.instance_44.setTransform(-12.3,865.15,1.3027,1.3027);
	this.instance_44.alpha = 0.8789;
	this.instance_44._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_44).wait(359).to({_off:false},0).to({scaleX:1.5476,scaleY:1.5476,x:695.4,y:711.8,alpha:0.5781},39).to({regY:0.1,scaleX:2.0228,scaleY:2.0228,x:1308.85,y:506.75,alpha:1},19).to({x:-13.1,y:938.75,alpha:0.5781},1).to({x:107.85,y:824.25,alpha:0.6914},27).to({regX:0.1,regY:0.3,scaleX:2.8434,scaleY:2.8434,rotation:9.9696,x:24.95,y:711.5,alpha:0.8086},12).wait(74).to({alpha:1},0).to({x:-272.45,y:193},13).wait(7).to({startPosition:0},0).to({y:-159.7},8).wait(125).to({startPosition:0},0).to({alpha:0},20).to({_off:true},1).wait(3355));

	// cloud6
	this.instance_45 = new lib.Tween12("synched",0);
	this.instance_45.setTransform(-183.7,884.55,1.5121,1.5121,0,0,0,-0.1,0.1);
	this.instance_45.alpha = 0.3086;
	this.instance_45._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_45).wait(359).to({_off:false},0).to({x:-142.65},21).to({x:79.85,y:800.3},16).to({scaleX:1.7147,scaleY:1.7147,x:269.95,y:433.05},10).to({scaleX:2.0134,scaleY:2.0134,x:67.55,y:-152.05},13).wait(112).to({startPosition:0},0).to({regX:0,regY:0,scaleX:2.5335,scaleY:2.5335,x:359.95,y:-415.5,alpha:1},13).to({y:-437.75},7).wait(133).to({startPosition:0},0).to({alpha:0},20).to({_off:true},1).wait(3355));

	// Cloud2
	this.instance_46 = new lib.Tween11("synched",0);
	this.instance_46.setTransform(-256.25,861.7);
	this.instance_46.alpha = 0.6914;
	this.instance_46._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_46).wait(359).to({_off:false},0).to({scaleX:1.144,scaleY:1.144,x:736.6,y:725.8,alpha:0.8398},39).to({regX:0.1,regY:0.1,scaleX:1.5554,scaleY:1.5554,x:1054.5,y:34.1,alpha:1},21).wait(112).to({startPosition:0},0).to({x:443.9,y:-252.25,alpha:0.5117},13).wait(140).to({startPosition:0},0).to({alpha:0},20).to({_off:true},1).wait(3355));

	// owl_txt_bg
	this.instance_47 = new lib.owl_text_bg("synched",0);
	this.instance_47.setTransform(501.05,524.9,1,1,0,0,0,0,324.3);
	this.instance_47.alpha = 0;
	this.instance_47._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_47).wait(470).to({_off:false},0).to({y:443.3,alpha:1},13).wait(48).to({startPosition:0},0).to({y:524.9,alpha:0},13).to({_off:true},1).wait(3515));

	// sky_alley
	this.sky_alley = new lib.sky_alley_2("synched",0);
	this.sky_alley.name = "sky_alley";
	this.sky_alley.setTransform(609.9,1335.6);
	this.sky_alley.alpha = 0;
	this.sky_alley._off = true;
	var sky_alleyFilter_3 = new cjs.ColorFilter(1,1,1,1,0,0,0,0);
	this.sky_alley.filters = [sky_alleyFilter_3];
	this.sky_alley.cache(-752,-560,1504,1120);

	this.timeline.addTween(cjs.Tween.get(this.sky_alley).wait(398).to({_off:false},0).to({scaleX:0.7609,scaleY:0.7609,x:527.8,y:424.6,alpha:1},21).to({startPosition:0},56).to({startPosition:0},7).wait(60).to({scaleX:0.9307,scaleY:0.9307},0).to({regY:0.1,scaleX:0.7798,scaleY:0.7798,x:513.15,y:192.55},17).wait(125).to({startPosition:0},0).to({startPosition:0},20).wait(107).to({startPosition:0},0).wait(23).to({startPosition:0},0).to({y:-608.2},20).to({_off:true},1).wait(3205));
	this.timeline.addTween(cjs.Tween.get(sky_alleyFilter_3).wait(398).to(new cjs.ColorFilter(1,1,1,1,0,0,0,0), 21).wait(123).to(new cjs.ColorFilter(1,1,1,1,0,0,0,0), 17).wait(125).to(new cjs.ColorFilter(0.72,0.72,0.72,1,71.4,71.4,71.4,0), 20).wait(3355));

	// night_sky
	this.instance_48 = new lib.sky_alley_gradient_1("synched",0);
	this.instance_48.setTransform(516.05,20.85,0.94,1.1651,0,0,0,-4,17.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_48).to({alpha:0},30).to({_off:true},1).wait(4029));

	// smog
	this.instance_49 = new lib.smog("synched",0);
	this.instance_49.setTransform(508.85,0,0.9849,1.6151,0,0,0,0.1,0);

	this.timeline.addTween(cjs.Tween.get(this.instance_49).wait(30).to({startPosition:0},0).to({alpha:0},29).to({_off:true},1).wait(4000));

	// word
	this.animal_text = new lib.animal_text("synched",0);
	this.animal_text.name = "animal_text";
	this.animal_text.setTransform(579.35,589.25);
	this.animal_text.alpha = 0;
	this.animal_text._off = true;

	this.timeline.addTween(cjs.Tween.get(this.animal_text).wait(258).to({_off:false},0).to({scaleX:0.8396,scaleY:0.8396,x:542.5,y:497.15,alpha:1},30).wait(110).to({scaleX:1,scaleY:1,x:579.35,y:589.25},0).to({x:583,y:702.45,alpha:0},7).to({_off:true},9).wait(3646));

	// Animals
	this.instance_50 = new lib.Tween1("synched",0);
	this.instance_50.setTransform(552.9,1140.7);
	this.instance_50._off = true;

	this.instance_51 = new lib.Tween2("synched",0);
	this.instance_51.setTransform(552.9,666.15);
	this.instance_51._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_50).wait(119).to({_off:false},0).to({y:856.35},34).wait(51).to({startPosition:0},0).to({_off:true,y:666.15},35).wait(3821));
	this.timeline.addTween(cjs.Tween.get(this.instance_51).wait(204).to({_off:false},35).to({scaleX:1.1745,scaleY:1.1745,y:573.75},18).to({scaleX:1.1841,scaleY:1.1841,y:577.6},1).to({y:505.15},30).to({startPosition:0},28).wait(82).to({startPosition:0},0).to({x:556.55,y:618.35,alpha:0},15).to({_off:true},1).wait(3646));

	// city_blur
	this.instance_52 = new lib.city_blur("synched",0);
	this.instance_52.setTransform(515,765.5,1,1,0,0,0,0,243.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_52).wait(59).to({x:516.35},0).wait(1).to({regX:0.9,regY:243.7,scaleX:1.0155,scaleY:1.0155,x:514.8,y:752.5},0).wait(1).to({scaleX:1.031,scaleY:1.031,x:511.3,y:739.35},0).wait(1).to({scaleX:1.0464,scaleY:1.0464,x:513.9,y:726.2},0).wait(1).to({scaleX:1.0619,scaleY:1.0619,x:514.6,y:713.05},0).wait(1).to({scaleX:1.0774,scaleY:1.0774,x:513.4,y:699.9},0).wait(1).to({scaleX:1.0929,scaleY:1.0929,x:511.35,y:686.75},0).wait(1).to({scaleX:1.1084,scaleY:1.1084,x:512.85,y:673.6},0).wait(1).to({scaleX:1.1238,scaleY:1.1238,x:513.45,y:660.45},0).wait(1).to({scaleX:1.1393,scaleY:1.1393,x:512.35,y:647.25,alpha:0.9375},0).wait(1).to({scaleX:1.1548,scaleY:1.1548,x:511.4,y:634.1,alpha:0.8526},0).wait(1).to({scaleX:1.1703,scaleY:1.1703,x:512.4,y:620.95,alpha:0.7708},0).wait(1).to({scaleX:1.1874,scaleY:1.1874,x:512.6,y:607.35,alpha:0.6923},0).wait(1).to({scaleX:1.2045,scaleY:1.2045,x:512.15,y:593.85,alpha:0.617},0).wait(1).to({scaleX:1.2216,scaleY:1.2216,x:511.45,y:580.25,alpha:0.5449},0).wait(1).to({scaleX:1.2388,scaleY:1.2388,x:511.15,y:566.7,alpha:0.476},0).wait(1).to({scaleX:1.2559,scaleY:1.2559,x:511.3,y:553.1,alpha:0.4103},0).wait(1).to({scaleX:1.273,scaleY:1.273,x:511.2,y:539.55,alpha:0.3478},0).wait(1).to({scaleX:1.2901,scaleY:1.2901,x:511.5,y:525.95,alpha:0.2885},0).wait(1).to({scaleX:1.3072,scaleY:1.3072,x:511.7,y:512.35,alpha:0.2324},0).wait(1).to({scaleX:1.3243,scaleY:1.3243,x:511.65,y:498.8,alpha:0.1795},0).wait(1).to({scaleX:1.3414,scaleY:1.3414,x:511.6,y:485.2,alpha:0.1298},0).wait(1).to({scaleX:1.3586,scaleY:1.3586,x:511.55,y:471.65,alpha:0.0834},0).wait(1).to({scaleX:1.3757,scaleY:1.3757,x:511.6,y:458.1,alpha:0.0401},0).wait(1).to({scaleX:1.3928,scaleY:1.3928,y:444.5,alpha:0},0).to({_off:true},1).wait(3976));

	// city
	this.instance_53 = new lib.city("synched",0);
	this.instance_53.setTransform(524,765.5,1,1,0,0,0,0,243.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_53).wait(59).to({startPosition:0},0).to({regX:0.1,scaleX:1.479,scaleY:1.479,x:511.05,y:464.55},24).to({_off:true},331).wait(3646));

	// sunrise
	this.instance_54 = new lib.sun("synched",0);
	this.instance_54.setTransform(514,788.95);
	this.instance_54.alpha = 0.5586;
	this.instance_54._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_54).wait(30).to({_off:false},0).to({regX:0.3,regY:-5.5,scaleX:1.748,scaleY:1.748,x:514.25,y:311.9,alpha:0},28).to({_off:true},1).wait(4001));

	// Sky
	this.instance_55 = new lib.city_sky("synched",0);
	this.instance_55.setTransform(511.95,383.5,0.6642,0.8649);

	this.city_sky = new lib.city_sky("synched",0);
	this.city_sky.name = "city_sky";
	this.city_sky.setTransform(511.95,383.5,0.6642,0.8649);
	this.city_sky._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_55}]}).to({state:[{t:this.city_sky}]},59).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[{t:this.city_sky}]},1).to({state:[]},1).wait(3642));
	this.timeline.addTween(cjs.Tween.get(this.city_sky).wait(59).to({_off:false},0).wait(1).to({regX:3.3,regY:4.7,scaleX:0.6682,scaleY:0.87,x:514.15,y:387.6},0).wait(1).to({scaleX:0.6721,scaleY:0.8752},0).wait(1).to({scaleX:0.6761,scaleY:0.8803,x:514.2,y:387.65},0).wait(1).to({scaleX:0.68,scaleY:0.8855},0).wait(1).to({scaleX:0.684,scaleY:0.8906,y:387.7},0).wait(1).to({scaleX:0.688,scaleY:0.8958},0).wait(1).to({scaleX:0.6919,scaleY:0.9009,x:514.25,y:387.75},0).wait(1).to({scaleX:0.6959,scaleY:0.9061},0).wait(1).to({scaleX:0.6998,scaleY:0.9113,y:387.8},0).wait(1).to({scaleX:0.7038,scaleY:0.9164},0).wait(1).to({scaleX:0.7077,scaleY:0.9216,x:514.3,y:387.85},0).wait(1).to({scaleX:0.7117,scaleY:0.9267},0).wait(1).to({scaleX:0.7156,scaleY:0.9319,y:387.9},0).wait(1).to({scaleX:0.7196,scaleY:0.937},0).wait(1).to({scaleX:0.7236,scaleY:0.9422,x:514.35,y:387.95},0).wait(1).to({scaleX:0.7275,scaleY:0.9473},0).wait(1).to({scaleX:0.7315,scaleY:0.9525,y:388},0).wait(1).to({scaleX:0.7354,scaleY:0.9576,x:514.4},0).wait(1).to({scaleX:0.7394,scaleY:0.9628,y:388.05},0).wait(1).to({scaleX:0.7433,scaleY:0.9679},0).wait(1).to({scaleX:0.7473,scaleY:0.9731},0).wait(1).to({scaleX:0.7513,scaleY:0.9782,x:514.45,y:388.1},0).wait(1).to({scaleX:0.7552,scaleY:0.9834},0).wait(1).to({scaleX:0.7592,scaleY:0.9885,y:388.15},0).wait(1).to({scaleX:0.7552,scaleY:0.9833,y:388.1},0).wait(1).to({scaleX:0.7512,scaleY:0.9781},0).wait(1).to({scaleX:0.7472,scaleY:0.9729,x:514.4,y:388.05},0).wait(1).to({scaleX:0.7432,scaleY:0.9677},0).wait(1).to({scaleX:0.7392,scaleY:0.9625,y:388},0).wait(1).to({scaleX:0.7352,scaleY:0.9573},0).wait(1).to({scaleX:0.7312,scaleY:0.9521,x:514.35},0).wait(1).to({scaleX:0.7272,scaleY:0.9469,y:387.95},0).wait(1).to({scaleX:0.7232,scaleY:0.9417},0).wait(1).to({scaleX:0.7192,scaleY:0.9365,x:514.3,y:387.9},0).wait(1).to({scaleX:0.7152,scaleY:0.9313},0).wait(1).to({scaleX:0.7113,scaleY:0.9261,y:387.85},0).wait(1).to({scaleX:0.7073,scaleY:0.921},0).wait(1).to({scaleX:0.7033,scaleY:0.9158,x:514.25,y:387.8},0).wait(1).to({scaleX:0.6993,scaleY:0.9106},0).wait(1).to({scaleX:0.6953,scaleY:0.9054,y:387.75},0).wait(1).to({scaleX:0.6913,scaleY:0.9002},0).wait(1).to({scaleX:0.6873,scaleY:0.895,x:514.2,y:387.7},0).wait(1).to({scaleX:0.6833,scaleY:0.8898},0).wait(1).to({scaleX:0.6793,scaleY:0.8846,y:387.65},0).wait(1).to({scaleX:0.6753,scaleY:0.8794},0).wait(1).to({scaleX:0.6713,scaleY:0.8742,x:514.15,y:387.6},0).wait(1).to({scaleX:0.6673,scaleY:0.869},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).to({_off:true},1).wait(3642));

	this.filterCacheList = [];
	this.filterCacheList.push({instance: this.instance_39, startFrame:834, endFrame:834, x:-623, y:-432, w:1246, h:865});
	this.filterCacheList.push({instance: this.instance_39, startFrame:0, endFrame:0, x:-623, y:-432, w:1246, h:865});
	this.filterCacheList.push({instance: this.instance_39, startFrame:845, endFrame:854, x:-623, y:-432, w:1246, h:865});
	this.filterCacheList.push({instance: this.instance_40, startFrame:845, endFrame:854, x:-623, y:-432, w:1246, h:865});
	this.filterCacheList.push({instance: this.instance_40, startFrame:864, endFrame:879, x:-623, y:-432, w:1246, h:865});
	this.filterCacheList.push({instance: this.sky_alley, startFrame:398, endFrame:398, x:-752, y:-560, w:1504, h:1120});
	this.filterCacheList.push({instance: this.sky_alley, startFrame:399, endFrame:419, x:-752, y:-560, w:1504, h:1120});
	this.filterCacheList.push({instance: this.sky_alley, startFrame:542, endFrame:542, x:-752, y:-560, w:1504, h:1120});
	this.filterCacheList.push({instance: this.sky_alley, startFrame:543, endFrame:559, x:-752, y:-560, w:1504, h:1120});
	this.filterCacheList.push({instance: this.sky_alley, startFrame:684, endFrame:684, x:-752, y:-560, w:1504, h:1120});
	this.filterCacheList.push({instance: this.sky_alley, startFrame:685, endFrame:704, x:-752, y:-560, w:1504, h:1120});
	this._renderFirstFrame();

}).prototype = p = new lib.AnMovieClip();
p.nominalBounds = new cjs.Rectangle(-1206.5,-678.2,3939.7,2801.8);
// library properties:
lib.properties = {
	id: '6F9624C44C354D5490851EA990C6744B',
	width: 1024,
	height: 768,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"images/nekocase_atlas_1.png?1638914737693", id:"nekocase_atlas_1"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['6F9624C44C354D5490851EA990C6744B'] = {
	getStage: function() { return exportRoot.stage; },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


an.makeResponsive = function(isResp, respDim, isScale, scaleType, domContainers) {		
	var lastW, lastH, lastS=1;		
	window.addEventListener('resize', resizeCanvas);		
	resizeCanvas();		
	function resizeCanvas() {			
		var w = lib.properties.width, h = lib.properties.height;			
		var iw = window.innerWidth, ih=window.innerHeight;			
		var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;			
		if(isResp) {                
			if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {                    
				sRatio = lastS;                
			}				
			else if(!isScale) {					
				if(iw<w || ih<h)						
					sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==1) {					
				sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==2) {					
				sRatio = Math.max(xRatio, yRatio);				
			}			
		}
		domContainers[0].width = w * pRatio * sRatio;			
		domContainers[0].height = h * pRatio * sRatio;
		domContainers.forEach(function(container) {				
			container.style.width = w * sRatio + 'px';				
			container.style.height = h * sRatio + 'px';			
		});
		stage.scaleX = pRatio*sRatio;			
		stage.scaleY = pRatio*sRatio;
		lastW = iw; lastH = ih; lastS = sRatio;            
		stage.tickOnUpdate = false;            
		stage.update();            
		stage.tickOnUpdate = true;		
	}
}
an.handleSoundStreamOnTick = function(event) {
	if(!event.paused){
		var stageChild = stage.getChildAt(0);
		if(!stageChild.paused || stageChild.ignorePause){
			stageChild.syncStreamSounds();
		}
	}
}
an.handleFilterCache = function(event) {
	if(!event.paused){
		var target = event.target;
		if(target){
			if(target.filterCacheList){
				for(var index = 0; index < target.filterCacheList.length ; index++){
					var cacheInst = target.filterCacheList[index];
					if((cacheInst.startFrame <= target.currentFrame) && (target.currentFrame <= cacheInst.endFrame)){
						cacheInst.instance.cache(cacheInst.x, cacheInst.y, cacheInst.w, cacheInst.h);
					}
				}
			}
		}
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;