(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [
		{name:"l1_atlas_1", frames: [[0,0,1500,1357]]},
		{name:"l1_atlas_2", frames: [[0,0,1500,1116]]},
		{name:"l1_atlas_3", frames: [[0,0,1500,1115],[0,1117,1600,906]]},
		{name:"l1_atlas_4", frames: [[0,1081,870,409],[872,1081,500,405],[0,629,1200,450],[1202,826,300,164],[0,0,1314,627],[1202,629,300,195]]}
];


(lib.AnMovieClip = function(){
	this.actionFrames = [];
	this.ignorePause = false;
	this.gotoAndPlay = function(positionOrLabel){
		cjs.MovieClip.prototype.gotoAndPlay.call(this,positionOrLabel);
	}
	this.play = function(){
		cjs.MovieClip.prototype.play.call(this);
	}
	this.gotoAndStop = function(positionOrLabel){
		cjs.MovieClip.prototype.gotoAndStop.call(this,positionOrLabel);
	}
	this.stop = function(){
		cjs.MovieClip.prototype.stop.call(this);
	}
}).prototype = p = new cjs.MovieClip();
// symbols:



(lib.alley = function() {
	this.initialize(ss["l1_atlas_3"]);
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.animals8 = function() {
	this.initialize(ss["l1_atlas_1"]);
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.city = function() {
	this.initialize(ss["l1_atlas_4"]);
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.cloud_1 = function() {
	this.initialize(ss["l1_atlas_4"]);
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.cloud_2 = function() {
	this.initialize(ss["l1_atlas_4"]);
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.cloud_3 = function() {
	this.initialize(ss["l1_atlas_4"]);
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.man = function() {
	this.initialize(ss["l1_atlas_4"]);
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.owl = function() {
	this.initialize(ss["l1_atlas_4"]);
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.sky_alley = function() {
	this.initialize(ss["l1_atlas_2"]);
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.sky_city_perspective = function() {
	this.initialize(ss["l1_atlas_3"]);
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.Tween15 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.man();
	this.instance.setTransform(-378,-181,0.5759,0.5759);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-378,-181,756.8,361.1);


(lib.Tween14 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.alley();
	this.instance.setTransform(-694.75,-516.45,0.9264,0.9264);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-694.7,-516.4,1389.5,1032.9);


(lib.Tween13 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.sky_alley();
	this.instance.setTransform(-750,-558);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-750,-558,1500,1116);


(lib.Tween12 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.cloud_1();
	this.instance.setTransform(-250,-202.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-250,-202.5,500,405);


(lib.Tween11 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.cloud_2();
	this.instance.setTransform(-665,-249,1.1079,1.1079);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-665,-249,1329.5,498.6);


(lib.Tween10 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.cloud_3();
	this.instance.setTransform(-150,-82);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-150,-82,300,164);


(lib.Tween8 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.cloud_3();
	this.instance.setTransform(-351.95,-192.4,2.3463,2.3463);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-351.9,-192.4,703.9,384.8);


(lib.Tween7 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.owl();
	this.instance.setTransform(-88,-57,0.5846,0.5846);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-88,-57,175.4,114);


(lib.Tween5 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.sky_city_perspective();
	this.instance.setTransform(-618,-448,1.271,1.271);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-618,-448,2033.7,1151.6);


(lib.Tween3 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.city();
	this.instance.setTransform(-594,-394,2.3638,2.3638);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-594,-394,2056.5,966.8);


(lib.Tween1 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.animals8();
	this.instance.setTransform(-28,-377,0.5559,0.5559);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-28,-377,833.8,754.4);


(lib.___Camera___ = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// timeline functions:
	this.frame_0 = function() {
		this.visible = false;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// cameraBoundary
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("rgba(0,0,0,0)").ss(2,1,1,3,true).p("EAq+AfQMhV7AAAMAAAg+fMBV7AAAg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(2));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-961,-541,1922,1082);


(lib.Scene_1_Sky = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Sky
	this.instance = new lib.Tween5("synched",0);
	this.instance.setTransform(-415.9,-72,5.6566,5.6566);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regY:-0.1,scaleX:4.6227,scaleY:4.6227,x:-1266.4,y:649.4},74).wait(11));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_owl = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// owl
	this.instance = new lib.Tween7("synched",0);
	this.instance.setTransform(-374.15,1845.25,2.7262,2.7262,-21.7295);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(79).to({_off:false},0).wait(1).to({regX:-0.3,scaleX:2.7263,scaleY:2.7263,rotation:-20.9501,x:-234.95,y:1771.9},0).wait(1).to({rotation:-20.1706,x:-81.35,y:1707.7},0).wait(1).to({rotation:-19.3912,x:69.1,y:1650.75},0).wait(1).to({rotation:-18.6118,x:210.65,y:1599.9},0).wait(1).to({rotation:-17.8323,x:341.95,y:1554.05},0).wait(1).to({rotation:-17.0529,x:462.9,y:1512.5},0).wait(1).to({rotation:-16.2735,x:574,y:1474.75},0).wait(1).to({rotation:-15.494,x:675.8,y:1440.35},0).wait(1).to({rotation:-14.7146,x:769.05,y:1409},0).wait(1).to({scaleX:2.7262,scaleY:2.7262,rotation:-13.9352,x:854.3,y:1380.45},0).wait(1).to({rotation:-13.1557,x:932.15,y:1354.45},0).wait(1).to({rotation:-12.3763,x:1003.05,y:1330.85},0).wait(1).to({scaleX:2.7263,scaleY:2.7263,rotation:-11.5969,x:1067.35,y:1309.4},0).wait(1).to({rotation:-10.8174,x:1126.4,y:1290.15},0).wait(1).to({rotation:-10.038,x:1181.1,y:1272.9},0).wait(1).to({rotation:-9.2585,x:1231.3,y:1257.6},0).wait(1).to({rotation:-8.4791,x:1276.6,y:1244.15},0).wait(1).to({rotation:-7.6997,x:1317,y:1232.55},0).wait(1).to({rotation:-6.9202,x:1352.4,y:1222.7},0).wait(1).to({scaleX:2.7262,scaleY:2.7262,rotation:-6.1408,x:1383.15,y:1214.6},0).wait(1).to({scaleX:2.7263,scaleY:2.7263,rotation:-3.7537,x:1409.9,y:1208.2},0).wait(1).to({rotation:-1.3666,x:1433.9,y:1203.5},0).wait(1).to({rotation:1.0205,x:1456.35,y:1200.55},0).wait(1).to({rotation:3.4077,x:1478.85,y:1199.3},0).wait(1).to({rotation:5.7948,x:1502.7,y:1199.8},0).wait(1).to({rotation:8.1819,x:1528.7,y:1202.15},0).wait(1).to({scaleX:2.7262,scaleY:2.7262,rotation:10.569,x:1557.6,y:1206.25},0).wait(1).to({scaleX:2.7263,scaleY:2.7263,rotation:12.9561,x:1589.5,y:1212.25},0).wait(1).to({rotation:15.3433,x:1624.6,y:1220.3},0).wait(1).to({rotation:17.7304,x:1662.8,y:1230.3},0).wait(1).to({rotation:20.1175,x:1704.05,y:1242.45},0).wait(1).to({rotation:22.5046,x:1748.1,y:1257},0).wait(1).to({rotation:24.8917,x:1794.85,y:1273.95},0).wait(1).to({scaleX:2.7262,scaleY:2.7262,rotation:27.2789,x:1844.2,y:1293.6},0).wait(1).to({rotation:29.666,x:1896.2,y:1316.2},0).wait(1).to({rotation:32.0531,x:1950.65,y:1342.1},0).wait(1).to({rotation:34.4402,x:2007.8,y:1371.75},0).wait(1).to({rotation:36.8273,x:2067.5,y:1405.65},0).wait(1).to({rotation:39.2145,x:2130.15,y:1444.7},0).wait(1).to({rotation:41.6016,x:2195.95,y:1489.9},0).wait(1).to({rotation:43.9887,x:2265.45,y:1543.05},0).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_cloud6dup = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// cloud6dup
	this.instance = new lib.Tween12("synched",0);
	this.instance.setTransform(-330.3,1930.8,1.9688,1.9688,0,0,0,-0.1,0.1);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(53).to({_off:false},0).to({x:2175,y:928.6},55).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_cloud6 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// cloud6
	this.instance = new lib.Tween12("synched",0);
	this.instance.setTransform(1530.8,2125.65,1.9688,1.9688,0,0,0,-0.1,0.1);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(59).to({_off:false},0).to({regY:0.2,x:1726.6,y:1380.6},16).to({scaleX:1.9687,scaleY:1.9687,x:2368.75,y:989},12).to({regY:0.1,scaleX:1.9688,scaleY:1.9688,x:2665.1,y:808.25},20).wait(2));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_cloud3 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// cloud3
	this.instance = new lib.Tween10("synched",0);
	this.instance.setTransform(889.7,1902.3,1.9091,1.9091);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(59).to({_off:false},0).to({scaleX:1.909,scaleY:1.909,x:1202.95,y:1021.15},28).to({x:1256.95,y:869.25},16).to({scaleX:1.9091,scaleY:1.9091,x:1267.8,y:838.9},4).wait(2));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Cloud2dup = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Cloud2dup
	this.instance = new lib.Tween11("synched",0);
	this.instance.setTransform(-377.65,1964.5,1.4668,1.4668);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(59).to({_off:false},0).to({scaleX:1.4326,scaleY:1.4326,x:-966.05,y:1447.1},9).to({scaleX:1.3885,scaleY:1.3885,x:814.15,y:908.5},19).to({scaleX:1.3775,scaleY:1.3775,x:1259.2,y:773.85},16).to({scaleX:1.3749,scaleY:1.3749,x:1942.15,y:790.85},4).wait(2));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Cloud2 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Cloud2
	this.instance = new lib.Tween11("synched",0);
	this.instance.setTransform(-377.65,1964.5,1.4668,1.4668);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(59).to({_off:false},0).to({scaleX:1.4379,scaleY:1.4379,x:673.6,y:1476.6},11).to({scaleX:1.3906,scaleY:1.3906,x:1625.05,y:962.3},17).to({scaleX:1.3775,scaleY:1.3775,x:1889.3,y:819.4},16).to({scaleX:1.3749,scaleY:1.3749,x:1942.15,y:790.85},4).wait(2));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Cloud = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Cloud
	this.instance = new lib.Tween8("synched",0);
	this.instance.setTransform(-360.1,1055.05);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(80).to({_off:false},0).to({x:-3.9,y:980.7},7).to({x:129.7,y:952.85},10).to({x:572.5,y:769.4},10).to({startPosition:0},2).to({_off:true},1).wait(94));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_City = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// City
	this.instance = new lib.Tween3("synched",0);
	this.instance.setTransform(-585.8,1419.8,3.5065,3.5065);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(74).to({startPosition:0},0).wait(11));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_bus = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// bus
	this.instance = new lib.Tween15("synched",0);
	this.instance.setTransform(-2634.7,5460,4.5985,4.5985,0,0,0,0,-0.1);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(127).to({_off:false},0).wait(1).to({regX:0.4,regY:-0.5,x:-2550.9,y:5458.8},0).wait(1).to({x:-2457.15,y:5459.45},0).wait(1).to({x:-2352.4,y:5460.15},0).wait(1).to({x:-2237.6,y:5460.8},0).wait(1).to({x:-2113.45,y:5461.5},0).wait(1).to({x:-1980.95,y:5462.15},0).wait(1).to({x:-1841.15,y:5462.85},0).wait(1).to({x:-1694.95,y:5463.5},0).wait(1).to({x:-1543.45,y:5464.15},0).wait(1).to({x:-1387.6,y:5464.85},0).wait(1).to({scaleX:4.5984,scaleY:4.5984,x:-1228.5,y:5465.5},0).wait(1).to({x:-1067.25,y:5466.2},0).wait(1).to({x:-904.85,y:5466.85},0).wait(1).to({x:-742.5,y:5467.55},0).wait(1).to({x:-581.2,y:5468.2},0).wait(1).to({x:-422.1,y:5468.85},0).wait(1).to({x:-266.3,y:5469.55},0).wait(1).to({x:-114.7,y:5470.2},0).wait(1).to({x:31.45,y:5470.9},0).wait(1).to({x:171.2,y:5471.55},0).wait(1).to({x:303.65,y:5472.25},0).wait(1).to({x:427.8,y:5472.9},0).wait(1).to({x:542.65,y:5473.55},0).wait(1).to({x:647.35,y:5474.25},0).wait(1).to({x:741.1,y:5474.9},0).wait(1).to({x:823.15,y:5475.6},0).wait(1).to({x:897.1,y:5476.25},0).wait(1).to({x:967.6,y:5476.95},0).wait(1).to({scaleX:4.5983,scaleY:4.5983,x:1034.75,y:5477.6},0).wait(1).to({x:1098.7,y:5478.3},0).wait(1).to({x:1159.7,y:5478.95},0).wait(1).to({x:1217.85,y:5479.6},0).wait(1).to({x:1273.25,y:5480.3},0).wait(1).to({x:1326.05,y:5480.95},0).wait(1).to({x:1376.55,y:5481.65},0).wait(1).to({x:1424.75,y:5482.3},0).wait(1).to({x:1470.8,y:5483},0).wait(1).to({x:1515,y:5483.65},0).wait(1).to({x:1557.3,y:5484.3},0).wait(1).to({x:1597.9,y:5485},0).wait(1).to({x:1637.1,y:5485.65},0).wait(1).to({x:1674.75,y:5486.35},0).wait(1).to({x:1711.3,y:5487},0).wait(1).to({x:1746.7,y:5487.7},0).wait(1).to({x:1781.1,y:5488.35},0).wait(1).to({scaleX:4.5982,scaleY:4.5982,x:1814.7,y:5489},0).wait(1).to({x:1847.55,y:5489.7},0).wait(1).to({x:1879.95,y:5490.35},0).wait(1).to({x:1911.9,y:5491.05},0).wait(1).to({x:1943.6,y:5491.7},0).wait(1).to({x:1975.15,y:5492.4},0).wait(1).to({x:2006.65,y:5493.05},0).wait(1).to({x:2038.25,y:5493.75},0).wait(1).to({x:2070.25,y:5494.4},0).wait(1).to({x:2102.6,y:5495.05},0).wait(1).to({x:2135.5,y:5495.75},0).wait(1).to({x:2169.05,y:5496.4},0).wait(1).to({x:2203.5,y:5497.1},0).wait(1).to({x:2238.85,y:5497.75},0).wait(1).to({x:2275.4,y:5498.45},0).wait(1).to({x:2313.15,y:5499.1},0).wait(1).to({x:2352.25,y:5499.75},0).wait(1).to({x:2392.9,y:5500.45},0).wait(1).to({scaleX:4.5981,scaleY:4.5981,x:2435.25,y:5501.1},0).wait(1).to({x:2479.35,y:5501.8},0).wait(1).to({x:2525.45,y:5502.45},0).wait(1).to({x:2573.65,y:5503.15},0).wait(1).to({x:2624.1,y:5503.8},0).wait(1).to({x:2676.95,y:5504.45},0).wait(1).to({x:2732.35,y:5505.15},0).wait(1).to({x:2790.45,y:5505.8},0).wait(1).to({x:2851.5,y:5506.5},0).wait(1).to({x:2915.45,y:5507.15},0).wait(1).to({x:2982.6,y:5507.85},0).wait(1).to({x:3053.1,y:5508.5},0).wait(1).to({x:3127.15,y:5509.15},0).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_bsky = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// bsky
	this.instance = new lib.Tween13("synched",0);
	this.instance.setTransform(863.8,2731.85,1.3599,1.3599);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(59).to({_off:false},0).to({x:924.5,y:1646.65},35).to({scaleX:5.9912,scaleY:5.9912,x:956.35,y:2165.1},8).to({scaleX:24.7665,scaleY:24.7665,x:1231.35,y:1033.65},14).wait(88));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Animals = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Animals
	this.instance = new lib.Tween1("synched",0);
	this.instance.setTransform(69.9,1870.25,2.3127,2.3127);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(74).to({startPosition:0},0).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Alley = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Alley
	this.instance = new lib.Tween14("synched",0);
	this.instance.setTransform(738.2,4803.05,4.093,4.093,0,0,0,0.1,0);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(102).to({_off:false},0).wait(102));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();


// stage content:
(lib.l1 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	this.actionFrames = [203];
	this.___GetDepth___ = function(obj) {
		var depth = obj.depth;
		var cameraObj = this.___camera___instance;
		if(cameraObj && cameraObj.depth && obj.isAttachedToCamera)
		{
			depth += depth + cameraObj.depth;
		}
		return depth;
		}
	this.___needSorting___ = function() {
		for (var i = 0; i < this.numChildren - 1; i++)
		{
			var prevDepth = this.___GetDepth___(this.getChildAt(i));
			var nextDepth = this.___GetDepth___(this.getChildAt(i + 1));
			if (prevDepth < nextDepth)
				return true;
		}
		return false;
	}
	this.___sortFunction___ = function(obj1, obj2) {
		return (this.exportRoot.___GetDepth___(obj2) - this.exportRoot.___GetDepth___(obj1));
	}
	this.on('tick', function (event){
		var curTimeline = event.currentTarget;
		if (curTimeline.___needSorting___()){
			this.sortChildren(curTimeline.___sortFunction___);
		}
	});

	// timeline functions:
	this.frame_203 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(203).call(this.frame_203).wait(1));

	// Camera
	this.___camera___instance = new lib.___Camera___();
	this.___camera___instance.name = "___camera___instance";
	this.___camera___instance.setTransform(960,540);
	this.___camera___instance.depth = 0;
	this.___camera___instance.visible = false;

	this.timeline.addTween(cjs.Tween.get(this.___camera___instance).to({x:915.15,y:1494.4},73).wait(45).to({x:689.2,y:5057.45},12).wait(74));

	// owl_obj_
	this.owl = new lib.Scene_1_owl();
	this.owl.name = "owl";
	this.owl.depth = 0;
	this.owl.isAttachedToCamera = 0
	this.owl.isAttachedToMask = 0
	this.owl.layerDepth = 0
	this.owl.layerIndex = 0
	this.owl.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.owl).wait(79).to({regX:-44.9,regY:954.4,x:-0.05},0).wait(1).to({regX:945.4,regY:1542.8,x:990.25,y:588.4},0).wait(40).to({_off:true},1).wait(83));

	// bus_obj_
	this.bus = new lib.Scene_1_bus();
	this.bus.name = "bus";
	this.bus.setTransform(0,0,0.6594,0.6594,0,0,0,-495.7,-278.8);
	this.bus.depth = 272.8;
	this.bus.isAttachedToCamera = 0
	this.bus.isAttachedToMask = 0
	this.bus.layerDepth = 0
	this.bus.layerIndex = 1
	this.bus.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.bus).wait(127).to({regX:-1364.8,regY:211.8,scaleX:0.4211,scaleY:0.4211,depth:726.3},0).wait(1).to({regX:246.9,regY:5483.9,scaleX:1,scaleY:1,x:1611.6,y:5272.1},0).wait(76));

	// Alley_obj_
	this.Alley = new lib.Scene_1_Alley();
	this.Alley.name = "Alley";
	this.Alley.depth = 0;
	this.Alley.isAttachedToCamera = 0
	this.Alley.isAttachedToMask = 0
	this.Alley.layerDepth = 0
	this.Alley.layerIndex = 2
	this.Alley.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Alley).wait(102).to({regX:-1669,regY:40.8,scaleX:0.3715,scaleY:0.3715,depth:893.7},0).wait(102));

	// cloud6dup_obj_
	this.cloud6dup = new lib.Scene_1_cloud6dup();
	this.cloud6dup.name = "cloud6dup";
	this.cloud6dup.depth = 0;
	this.cloud6dup.isAttachedToCamera = 0
	this.cloud6dup.isAttachedToMask = 0
	this.cloud6dup.layerDepth = 0
	this.cloud6dup.layerIndex = 3
	this.cloud6dup.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.cloud6dup).wait(53).to({regX:-44.9,regY:954.4,x:-0.05},55).to({_off:true},1).wait(95));

	// Cloud_obj_
	this.Cloud = new lib.Scene_1_Cloud();
	this.Cloud.name = "Cloud";
	this.Cloud.depth = 0;
	this.Cloud.isAttachedToCamera = 0
	this.Cloud.isAttachedToMask = 0
	this.Cloud.layerDepth = 0
	this.Cloud.layerIndex = 4
	this.Cloud.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Cloud).wait(80).to({regX:-44.9,regY:954.4,x:-0.05},0).wait(124));

	// Cloud2_obj_
	this.Cloud2 = new lib.Scene_1_Cloud2();
	this.Cloud2.name = "Cloud2";
	this.Cloud2.depth = 0;
	this.Cloud2.isAttachedToCamera = 0
	this.Cloud2.isAttachedToMask = 0
	this.Cloud2.layerDepth = 0
	this.Cloud2.layerIndex = 5
	this.Cloud2.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Cloud2).wait(70).to({regX:-44.9,regY:954.4,x:-0.05},17).wait(20).to({_off:true},2).wait(95));

	// Cloud2dup_obj_
	this.Cloud2dup = new lib.Scene_1_Cloud2dup();
	this.Cloud2dup.name = "Cloud2dup";
	this.Cloud2dup.depth = 0;
	this.Cloud2dup.isAttachedToCamera = 0
	this.Cloud2dup.isAttachedToMask = 0
	this.Cloud2dup.layerDepth = 0
	this.Cloud2dup.layerIndex = 6
	this.Cloud2dup.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Cloud2dup).wait(68).to({regX:-44.9,regY:954.4,x:-0.05},19).wait(20).to({_off:true},2).wait(95));

	// cloud3_obj_
	this.cloud3 = new lib.Scene_1_cloud3();
	this.cloud3.name = "cloud3";
	this.cloud3.depth = 0;
	this.cloud3.isAttachedToCamera = 0
	this.cloud3.isAttachedToMask = 0
	this.cloud3.layerDepth = 0
	this.cloud3.layerIndex = 7
	this.cloud3.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.cloud3).wait(59).to({regX:-44.9,regY:954.4,x:-0.05},28).wait(20).to({_off:true},2).wait(95));

	// cloud6_obj_
	this.cloud6 = new lib.Scene_1_cloud6();
	this.cloud6.name = "cloud6";
	this.cloud6.depth = 0;
	this.cloud6.isAttachedToCamera = 0
	this.cloud6.isAttachedToMask = 0
	this.cloud6.layerDepth = 0
	this.cloud6.layerIndex = 8
	this.cloud6.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.cloud6).wait(59).to({regX:-44.9,regY:954.4,x:-0.05},16).wait(32).to({_off:true},2).wait(95));

	// bsky_obj_
	this.bsky = new lib.Scene_1_bsky();
	this.bsky.name = "bsky";
	this.bsky.depth = 0;
	this.bsky.isAttachedToCamera = 0
	this.bsky.isAttachedToMask = 0
	this.bsky.layerDepth = 0
	this.bsky.layerIndex = 9
	this.bsky.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.bsky).wait(59).to({regX:-44.9,regY:954.4,x:-0.05},35).to({regX:-3314.2,regY:-884.5,scaleX:0.227,scaleY:0.227,x:0,depth:1798.95},8).to({regX:-16569.8,regY:-8340.5,scaleX:0.0549,scaleY:0.0549,x:0.25,y:0.1,depth:9092},14).wait(88));

	// Animals_obj_
	this.Animals = new lib.Scene_1_Animals();
	this.Animals.name = "Animals";
	this.Animals.setTransform(969.3,1870.65,0.6093,0.6093,0,0,0,975.4,2724);
	this.Animals.depth = 338.7;
	this.Animals.isAttachedToCamera = 0
	this.Animals.isAttachedToMask = 0
	this.Animals.layerDepth = 0
	this.Animals.layerIndex = 10
	this.Animals.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Animals).wait(74).to({regX:933.2,regY:4068.2,scaleX:0.517,scaleY:0.517,x:969.25,y:1870.6,depth:493.5},0).to({_off:true},1).wait(129));

	// City_obj_
	this.City = new lib.Scene_1_City();
	this.City.name = "City";
	this.City.setTransform(936.85,1733.25,0.2852,0.2852,0,0,0,878.9,4724.2);
	this.City.depth = 1324.05;
	this.City.isAttachedToCamera = 0
	this.City.isAttachedToMask = 0
	this.City.layerDepth = 0
	this.City.layerIndex = 11
	this.City.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.City).wait(74).to({regX:834,regY:5678.6},0).to({_off:true},11).wait(119));

	// Sky_obj_
	this.Sky = new lib.Scene_1_Sky();
	this.Sky.name = "Sky";
	this.Sky.setTransform(1840.1,650.75,0.229,0.229,0,0,0,4803.4,1023.8);
	this.Sky.depth = 1778.4;
	this.Sky.isAttachedToCamera = 0
	this.Sky.isAttachedToMask = 0
	this.Sky.layerDepth = 0
	this.Sky.layerIndex = 12
	this.Sky.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Sky).to({regX:4758.4,regY:1978.2},74).to({_off:true},11).wait(119));

	this._renderFirstFrame();

}).prototype = p = new lib.AnMovieClip();
p.nominalBounds = new cjs.Rectangle(-16383.5,-12246,36189.8,27099.4);
// library properties:
lib.properties = {
	id: '6F9624C44C354D5490851EA990C6744B',
	width: 1920,
	height: 1080,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"images/l1_atlas_1.png", id:"l1_atlas_1"},
		{src:"images/l1_atlas_2.png", id:"l1_atlas_2"},
		{src:"images/l1_atlas_3.png", id:"l1_atlas_3"},
		{src:"images/l1_atlas_4.png", id:"l1_atlas_4"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['6F9624C44C354D5490851EA990C6744B'] = {
	getStage: function() { return exportRoot.stage; },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}

p._getProjectionMatrix = function(container, totalDepth) {	var focalLength = 528.25;
	var projectionCenter = { x : lib.properties.width/2, y : lib.properties.height/2 };
	var scale = (totalDepth + focalLength)/focalLength;
	var scaleMat = new createjs.Matrix2D;
	scaleMat.a = 1/scale;
	scaleMat.d = 1/scale;
	var projMat = new createjs.Matrix2D;
	projMat.tx = -projectionCenter.x;
	projMat.ty = -projectionCenter.y;
	projMat = projMat.prependMatrix(scaleMat);
	projMat.tx += projectionCenter.x;
	projMat.ty += projectionCenter.y;
	return projMat;
}
p._handleTick = function(event) {
	var cameraInstance = exportRoot.___camera___instance;
	if(cameraInstance !== undefined && cameraInstance.pinToObject !== undefined)
	{
		cameraInstance.x = cameraInstance.pinToObject.x + cameraInstance.pinToObject.pinOffsetX;
		cameraInstance.y = cameraInstance.pinToObject.y + cameraInstance.pinToObject.pinOffsetY;
		if(cameraInstance.pinToObject.parent !== undefined && cameraInstance.pinToObject.parent.depth !== undefined)
		cameraInstance.depth = cameraInstance.pinToObject.parent.depth + cameraInstance.pinToObject.pinOffsetZ;
	}
	stage._applyLayerZDepth(exportRoot);
}
p._applyLayerZDepth = function(parent)
{
	var cameraInstance = parent.___camera___instance;
	var focalLength = 528.25;
	var projectionCenter = { 'x' : 0, 'y' : 0};
	if(parent === exportRoot)
	{
		var stageCenter = { 'x' : lib.properties.width/2, 'y' : lib.properties.height/2 };
		projectionCenter.x = stageCenter.x;
		projectionCenter.y = stageCenter.y;
	}
	for(child in parent.children)
	{
		var layerObj = parent.children[child];
		if(layerObj == cameraInstance)
			continue;
		stage._applyLayerZDepth(layerObj, cameraInstance);
		if(layerObj.layerDepth === undefined)
			continue;
		if(layerObj.currentFrame != layerObj.parent.currentFrame)
		{
			layerObj.gotoAndPlay(layerObj.parent.currentFrame);
		}
		var matToApply = new createjs.Matrix2D;
		var cameraMat = new createjs.Matrix2D;
		var totalDepth = layerObj.layerDepth ? layerObj.layerDepth : 0;
		var cameraDepth = 0;
		if(cameraInstance && !layerObj.isAttachedToCamera)
		{
			var mat = cameraInstance.getMatrix();
			mat.tx -= projectionCenter.x;
			mat.ty -= projectionCenter.y;
			cameraMat = mat.invert();
			cameraMat.prependTransform(projectionCenter.x, projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
			cameraMat.appendTransform(-projectionCenter.x, -projectionCenter.y, 1, 1, 0, 0, 0, 0, 0);
			if(cameraInstance.depth)
				cameraDepth = cameraInstance.depth;
		}
		if(layerObj.depth)
		{
			totalDepth = layerObj.depth;
		}
		//Offset by camera depth
		totalDepth -= cameraDepth;
		if(totalDepth < -focalLength)
		{
			matToApply.a = 0;
			matToApply.d = 0;
		}
		else
		{
			if(layerObj.layerDepth)
			{
				var sizeLockedMat = stage._getProjectionMatrix(parent, layerObj.layerDepth);
				if(sizeLockedMat)
				{
					sizeLockedMat.invert();
					matToApply.prependMatrix(sizeLockedMat);
				}
			}
			matToApply.prependMatrix(cameraMat);
			var projMat = stage._getProjectionMatrix(parent, totalDepth);
			if(projMat)
			{
				matToApply.prependMatrix(projMat);
			}
		}
		layerObj.transformMatrix = matToApply;
	}
}
an.makeResponsive = function(isResp, respDim, isScale, scaleType, domContainers) {		
	var lastW, lastH, lastS=1;		
	window.addEventListener('resize', resizeCanvas);		
	resizeCanvas();		
	function resizeCanvas() {			
		var w = lib.properties.width, h = lib.properties.height;			
		var iw = window.innerWidth, ih=window.innerHeight;			
		var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;			
		if(isResp) {                
			if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {                    
				sRatio = lastS;                
			}				
			else if(!isScale) {					
				if(iw<w || ih<h)						
					sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==1) {					
				sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==2) {					
				sRatio = Math.max(xRatio, yRatio);				
			}			
		}
		domContainers[0].width = w * pRatio * sRatio;			
		domContainers[0].height = h * pRatio * sRatio;
		domContainers.forEach(function(container) {				
			container.style.width = w * sRatio + 'px';				
			container.style.height = h * sRatio + 'px';			
		});
		stage.scaleX = pRatio*sRatio;			
		stage.scaleY = pRatio*sRatio;
		lastW = iw; lastH = ih; lastS = sRatio;            
		stage.tickOnUpdate = false;            
		stage.update();            
		stage.tickOnUpdate = true;		
	}
}

// Virtual camera API : 

an.VirtualCamera = new function() {
var _camera = new Object();
function VC(timeline) {
	this.timeline = timeline;
	this.camera = timeline.___camera___instance;
	this.centerX = lib.properties.width / 2;
	this.centerY = lib.properties.height / 2;
	this.camAxisX = this.camera.x;
	this.camAxisY = this.camera.y;
	if(timeline.___camera___instance == null || timeline.___camera___instance == undefined ) {
		timeline.___camera___instance = new cjs.MovieClip();
		timeline.___camera___instance.visible = false;
		timeline.___camera___instance.parent = timeline;
		timeline.___camera___instance.setTransform(this.centerX, this.centerY);
	}
	this.camera = timeline.___camera___instance;
}

VC.prototype.moveBy = function(x, y, z) {
z = typeof z !== 'undefined' ? z : 0;
	var position = this.___getCamPosition___();
	var rotAngle = this.getRotation()*Math.PI/180;
	var sinTheta = Math.sin(rotAngle);
	var cosTheta = Math.cos(rotAngle);
	var offX= x*cosTheta + y*sinTheta;
	var offY = y*cosTheta - x*sinTheta;
	this.camAxisX = this.camAxisX - x;
	this.camAxisY = this.camAxisY - y;
	var posX = position.x + offX;
	var posY = position.y + offY;
	this.camera.x = this.centerX - posX;
	this.camera.y = this.centerY - posY;
	this.camera.depth += z;
};

VC.prototype.setPosition = function(x, y, z) {
	z = typeof z !== 'undefined' ? z : 0;

	const MAX_X = 10000;
	const MIN_X = -10000;
	const MAX_Y = 10000;
	const MIN_Y = -10000;
	const MAX_Z = 10000;
	const MIN_Z = -5000;

	if(x > MAX_X)
	  x = MAX_X;
	else if(x < MIN_X)
	  x = MIN_X;
	if(y > MAX_Y)
	  y = MAX_Y;
	else if(y < MIN_Y)
	  y = MIN_Y;
	if(z > MAX_Z)
	  z = MAX_Z;
	else if(z < MIN_Z)
	  z = MIN_Z;

	var rotAngle = this.getRotation()*Math.PI/180;
	var sinTheta = Math.sin(rotAngle);
	var cosTheta = Math.cos(rotAngle);
	var offX= x*cosTheta + y*sinTheta;
	var offY = y*cosTheta - x*sinTheta;
	
	this.camAxisX = this.centerX - x;
	this.camAxisY = this.centerY - y;
	this.camera.x = this.centerX - offX;
	this.camera.y = this.centerY - offY;
	this.camera.depth = z;
};

VC.prototype.getPosition = function() {
	var loc = new Object();
	loc['x'] = this.centerX - this.camAxisX;
	loc['y'] = this.centerY - this.camAxisY;
	loc['z'] = this.camera.depth;
	return loc;
};

VC.prototype.resetPosition = function() {
	this.setPosition(0, 0);
};

VC.prototype.zoomBy = function(zoom) {
	this.setZoom( (this.getZoom() * zoom) / 100);
};

VC.prototype.setZoom = function(zoom) {
	const MAX_zoom = 10000;
	const MIN_zoom = 1;
	if(zoom > MAX_zoom)
	zoom = MAX_zoom;
	else if(zoom < MIN_zoom)
	zoom = MIN_zoom;
	this.camera.scaleX = 100 / zoom;
	this.camera.scaleY = 100 / zoom;
};

VC.prototype.getZoom = function() {
	return 100 / this.camera.scaleX;
};

VC.prototype.resetZoom = function() {
	this.setZoom(100);
};

VC.prototype.rotateBy = function(angle) {
	this.setRotation( this.getRotation() + angle );
};

VC.prototype.setRotation = function(angle) {
	const MAX_angle = 180;
	const MIN_angle = -179;
	if(angle > MAX_angle)
		angle = MAX_angle;
	else if(angle < MIN_angle)
		angle = MIN_angle;
	this.camera.rotation = -angle;
};

VC.prototype.getRotation = function() {
	return -this.camera.rotation;
};

VC.prototype.resetRotation = function() {
	this.setRotation(0);
};

VC.prototype.reset = function() {
	this.resetPosition();
	this.resetZoom();
	this.resetRotation();
	this.unpinCamera();
};
VC.prototype.setZDepth = function(zDepth) {
	const MAX_zDepth = 10000;
	const MIN_zDepth = -5000;
	if(zDepth > MAX_zDepth)
		zDepth = MAX_zDepth;
	else if(zDepth < MIN_zDepth)
		zDepth = MIN_zDepth;
	this.camera.depth = zDepth;
}
VC.prototype.getZDepth = function() {
	return this.camera.depth;
}
VC.prototype.resetZDepth = function() {
	this.camera.depth = 0;
}

VC.prototype.pinCameraToObject = function(obj, offsetX, offsetY, offsetZ) {

	offsetX = typeof offsetX !== 'undefined' ? offsetX : 0;

	offsetY = typeof offsetY !== 'undefined' ? offsetY : 0;

	offsetZ = typeof offsetZ !== 'undefined' ? offsetZ : 0;
	if(obj === undefined)
		return;
	this.camera.pinToObject = obj;
	this.camera.pinToObject.pinOffsetX = offsetX;
	this.camera.pinToObject.pinOffsetY = offsetY;
	this.camera.pinToObject.pinOffsetZ = offsetZ;
};

VC.prototype.setPinOffset = function(offsetX, offsetY, offsetZ) {
	if(this.camera.pinToObject != undefined) {
	this.camera.pinToObject.pinOffsetX = offsetX;
	this.camera.pinToObject.pinOffsetY = offsetY;
	this.camera.pinToObject.pinOffsetZ = offsetZ;
	}
};

VC.prototype.unpinCamera = function() {
	this.camera.pinToObject = undefined;
};
VC.prototype.___getCamPosition___ = function() {
	var loc = new Object();
	loc['x'] = this.centerX - this.camera.x;
	loc['y'] = this.centerY - this.camera.y;
	loc['z'] = this.depth;
	return loc;
};

this.getCamera = function(timeline) {
	timeline = typeof timeline !== 'undefined' ? timeline : null;
	if(timeline === null) timeline = exportRoot;
	if(_camera[timeline] == undefined)
	_camera[timeline] = new VC(timeline);
	return _camera[timeline];
}

this.getCameraAsMovieClip = function(timeline) {
	timeline = typeof timeline !== 'undefined' ? timeline : null;
	if(timeline === null) timeline = exportRoot;
	return this.getCamera(timeline).camera;
}
}


// Layer depth API : 

an.Layer = new function() {
	this.getLayerZDepth = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth; else 0;";
		return eval(script);
	}
	this.setLayerZDepth = function(timeline, layerName, zDepth)
	{
		const MAX_zDepth = 10000;
		const MIN_zDepth = -5000;
		if(zDepth > MAX_zDepth)
			zDepth = MAX_zDepth;
		else if(zDepth < MIN_zDepth)
			zDepth = MIN_zDepth;
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth = " + zDepth + ";";
		eval(script);
	}
	this.removeLayer = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline.removeChild(timeline." + layerName + ");";
		eval(script);
	}
	this.addNewLayer = function(timeline, layerName, zDepth)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		zDepth = typeof zDepth !== 'undefined' ? zDepth : 0;
		var layer = new createjs.MovieClip();
		layer.name = layerName;
		layer.depth = zDepth;
		layer.layerIndex = 0;
		timeline.addChild(layer);
	}
}
an.handleSoundStreamOnTick = function(event) {
	if(!event.paused){
		var stageChild = stage.getChildAt(0);
		if(!stageChild.paused || stageChild.ignorePause){
			stageChild.syncStreamSounds();
		}
	}
}
an.handleFilterCache = function(event) {
	if(!event.paused){
		var target = event.target;
		if(target){
			if(target.filterCacheList){
				for(var index = 0; index < target.filterCacheList.length ; index++){
					var cacheInst = target.filterCacheList[index];
					if((cacheInst.startFrame <= target.currentFrame) && (target.currentFrame <= cacheInst.endFrame)){
						cacheInst.instance.cache(cacheInst.x, cacheInst.y, cacheInst.w, cacheInst.h);
					}
				}
			}
		}
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;