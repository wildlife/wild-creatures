// We will keep track of where in the page we are in this page,
// and perform events accordingly

// import { setPlayingSong } from "./player";
function setPlayingSong(p) {
  console.log(p);
  document.getElementById("musicname").innerText = p;
}

// // fetch an audio as a blob
// let audioBlobURL = "";
// let musicHowls = []
// function fetchAudio() {

//   fetch("music/FULL12.mp3").then(response => {
//     response.blob().then(blob=>{
//       let url = URL.createObjectURL(blob);
//       audioBlobURL = url;
//       musicHowls = musicPaths.map((path,index)=>new Howl({src:[audioBlobURL],format:"mp3", loop:true, sprite:{"single":[index*30000,30000, true]} }));
//       console.log(musicHowls)
//     });
// fetchAudio();

let musicFiles = [
  "imananimal",
  "ladypilot",
  "hallsofsarah",
  "man",
  "starwitness",
  "wishiwasthemoon",
  "thenexttime",
  "favorite",
  "hellon",
  "holdon",
  "polarnettles",
  "wildcreatures",
  "furnace",
  "widow",
  "deepredbells",
  "thistornado",
  "winnie",
];
let musictransitionframes = [
  0, 454, 545, 685, 1027, 1254, 1622, 1810, 2087, 2512, 2855, 3145, 3607, 3771,
  3915,
];
let musicPaths = musicFiles.map((file) => `music/${file}.wav`);
let songName = [
  "I'm an Animal",
  "Lady Pilot",
  "Halls of Sarah",
  "Man",
  "Star Witness",
  "I Wish I Were The Moon",
  "The Next Time",
  "Favorite",
  "Hell On",
  "Hold On, Hold On",
  "Polar Nettles",
  "Wild Creatures",
  "Furnace Room Lullaby",
  "A Widow's Toast",
  "Deep Red Bells",
  "The Tornado Loves You",
  "Winnie",
];

Howler.unload();
Howler.volume(0);
// let musicHowls = musicPaths.map(path=>new Howl({src:[path], loop:true}));
let musicHowls;
let musicHowl = new Howl({ src: "music/ALL.mp3" });
musicHowl.on("load", () => {
  console.log("loaded");
  window.MUSICLOADED = true;
  if (window.IMAGELOADED) {
    let loadingoverlay = document.getElementById("loadingoverlay");
    if (loadingoverlay.style.display === "none") return;
    console.log("Loaded. Removing modal");
    loadingoverlay.style.display = "none";
    // When the modal is hidden...
    const scrollY = document.body.style.top;
    document.body.style.position = "";
    document.body.style.top = "";
    window.scrollTo(0, parseInt(scrollY || "0") * -1);
  }
  musicHowls = musicPaths.map(
    (path, index) =>
      new Howl({
        src: ["music/ALL.mp3"],
        loop: true,
        // html5:true, //??
        sprite: { single: [index * 30000, 30000, true] },
      })
  );
});

// function spritegen(){
//   let sprite = {};
//   for (let i = 0; i < 12; i++){
//     sprite[i] = [i*3000, (i+1)*3000]
//   }
//   return sprite;
// }
// var soundSprites = new Howl({
//   src: ['music/FULL12.mp3'],
//   sprite: spritegen()
// });

let muted = true;
let volumeWhenUnmute = 0;
let playing = true;
function toggle_mute(e) {
  let mutebutton = document.getElementById("MuteButton");
  let mutetext = document.getElementById("MuteText");
  mutebutton.classList.remove("blinking");
  if (muted) {
    muted = false;
    Howler.volume(volumeWhenUnmute);
    mutetext.innerText = "pause_circle";
  } else {
    muted = true;
    Howler.volume(0);
    mutetext.innerHTML = "play_circle";
  }
  return muted;
}

function toggle_play() {
  if (musicHowls === undefined) return;
  if (playing) {
    playing = false;
    musicHowls.forEach((song) => song.playing() && song.pause());
  } else {
    playing = true;
    musicHowls[curStateIndex]?.play();
  }
  return playing;
}

function setMotionParameters(stateIndex, { direction, progress }) {
  console.log(stateIndex, progress, direction);
  curStateIndex = stateIndex;

  if (musicHowls === undefined) return;

  if (musicHowls.length === 0) return;
  if (playing) {
    if (!musicHowls[stateIndex].playing()) {
      musicHowls[stateIndex].play("single");
    }
    musicHowls.forEach(
      (song, index) => index !== stateIndex && song.playing() && song.stop()
    );
  }
  setPlayingSong(songName[stateIndex]);

  if (progress < 0.1) {
    if (muted) volumeWhenUnmute = progress * 10;
    else Howler.volume(progress * 10);
  } else if (progress > 0.9) {
    if (muted) volumeWhenUnmute = (1 - progress) * 10;
    else Howler.volume((1 - progress) * 10);
  }
}

document.addEventListener("scroll", (e) => {
  let docHeight = document.body.offsetHeight;
  let winHeight = window.innerHeight;
  let scrollPercent = window.scrollY / (docHeight - winHeight);
  let curframe = scrollPercent * window.NUMBEROFFRAMES;

  musictransitionframes.forEach((frame, index) => {
    let nextframe = musictransitionframes[index + 1] ?? window.NUMBEROFFRAMES;
    if (curframe >= frame && curframe < nextframe) {
      setMotionParameters(index, {
        progress: (curframe - frame) / (nextframe - frame),
      });
    }
  });
});

// export { nextState, prevState, setMotionParameters, toggle_play, toggle_mute };
